$(document).ready(() => {

    /* Variable declaration - initialization */
    const table = $(document).find('table');

    const dt = table.DataTable({
        order: [[13, "desc"]],
        "columnDefs": [
            { "orderable": false, "targets": [0, 11] },
            { "searchable": false, "targets": [0, 2, 5] },
            { "className": "hidden", "targets": [2 , 5] }
        ]
    });
});
