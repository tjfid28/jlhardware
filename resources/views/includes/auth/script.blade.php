<!-- Jquery Core Js -->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ asset('js/backend/waves.min.js') }}"></script>

<!-- Validation Plugin Js -->
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>

<!-- Custom Js -->
<script src="{{ asset('js/backend/admin.js') }}"></script>

<!-- Additional script -->
@yield('scripts')
