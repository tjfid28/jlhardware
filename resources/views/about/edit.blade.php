@extends('layouts.backend')

@section('title', 'About')

@section('styles')
    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- froala editor CSS -->
    <link href="{{ asset('css/backend/froala_editor.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        ABOUT
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Update content</h2>
                </div>

                <div class="body">
                    <form class="editForm" action="{{ route('about.update') }}" method="PATCH">
                        <label for="content">Content*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="4" name="content">{{ $about->content }}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-lg btn-success btnUpdate waves-effect">UPDATE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- froala editor Js -->
    <script src="{{ asset('js/backend/froala_editor.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/about.js') }}"></script>
@endsection
