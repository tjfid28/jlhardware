<?php

namespace App\Notifications;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderApproved extends Notification
{
    private $order;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Order {$this->order->getOrderNumber()} approved")
            ->from(env('MAIL_USERNAME', 'noreply.jlhardware@gmail.com'), env('MAIL_NAME', 'no-reply@jlhardware'))
            ->line("Hello, Your order {$this->order->getOrderNumber()} has been approved and is ready To Ship.")
            ->line('Thank You for purchasing.')
            ->line("Approved by " . auth()->user()->name)
            ->line('This email is auto generated, please do not reply.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
