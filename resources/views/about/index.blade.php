@extends('layouts.app')

@section('title', 'About Us')

@section('content')
    <section class="bgimage bg-imageURL-about">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>About Us</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="default-margin">
        <div class="container">
            <div class="row">
                <div id="id-about" class="col-md-12">
                    {!! $about->content !!}
                </div>
            </div>
        </div>
    </section>
@endsection
