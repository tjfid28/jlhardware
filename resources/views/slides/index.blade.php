@extends('layouts.backend')

@section('title', 'Slides')

@section('styles')
    <!-- JQuery dataTables CSS -->
    <link href="{{ asset('css/backend/datatables.min.css') }}" rel="stylesheet">

    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        SLIDES
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-xs-6">
                            <h2>List of Slides</h2>
                        </div>

                        <div class="col-md-6 col-xs-6 text-right">
                            <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".addModal">
                                <i class="material-icons">add</i>
                                <span>Add new slide</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Actions</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Created At</th>
                                    <th class="text-center">Updated At</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse ($slides as $slide)
                                    <tr class="row{{ $slide->id }}">
                                        <td class="btnActionContainer">
                                            {{-- <button type="button" data-id="{{ $slide->id }}" class="btn btn-success btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".editModal" title="edit">
                                                <i class="material-icons">edit</i>
                                            </button> --}}
                                            <button type="button" data-id="{{ $slide->id }}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                        <td class="image-{{ $slide->id }}">
                                            <a href="{{ $slide->getImageUrl() }}" target="_blank">
                                                <img src="{{ $slide->getImageUrl() }}" alt="image" width="117" height="32">
                                            </a>
                                        </td>
                                        <td data-order="{{ $slide->created_at }}">{{ $slide->created_at }}</td>
                                        <td class="updated_at-{{ $slide->id }}" data-order="{{ $slide->updated_at }}">{{ $slide->updated_at }}</td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade addModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Add new slide</h4>
                </div>
                <form class="addForm" action="{{ route('slides.store') }}" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Image*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" class="form-control" id="add-image" name="image" accept="image/jpg,image/jpeg,image/png">
                                    </div>
                                    <div class="help-info">Image: jpg|jpeg|png, Min. width: 2000 pixels, Min. height: 547 pixels</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnSave waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- <div class="modal fade editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Edit slide</h4>
                </div>
                <form class="editForm" method="PATCH" enctype="multipart/form-data">
                    <div class="modal-body">
                        Alerts
                        <input class="edit-id" type="hidden" name="id">

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Image*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" class="form-control" name="image" accept="image/jpg,image/jpeg,image/png">
                                    </div>
                                    <div class="help-info">Image: jpg|jpeg|png, Min. width: 2000, Min. height: 547</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnUpdate waves-effect">UPDATE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div> -->

    <div class="modal fade deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Delete slide</h4>
                </div>
                <form class="deleteForm form-horizontal" method="DELETE">
                    <div class="modal-body">
                        <h5>Are you sure you want to delete this data?</h5>
                        <input class="delete-id" type="hidden" name="id">

                        <div class="row clearfix">
                            <div class="col-md-12">
                                <img class="img-responsive thumbnail delete-image" src="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnDelete waves-effect">DELETE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- JQuery dataTables JS -->
    <script type="text/javascript" src="{{ asset('js/backend/datatables.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/slides.js') }}"></script>
@endsection
