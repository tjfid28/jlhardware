<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff')->except('index');
    }

    public function index()
    {
        $contact = Contact::find(1);
        return view('contacts.index', compact('contact'));
    }

    public function edit()
    {
        $contact = Contact::find(1);
        return view('contacts.edit', compact('contact'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required|string|max:191',
            'mobile_no' => 'required|digits:10',
            'telephone_no' => 'nullable|digits:7',
            'email' => 'required|email',
            'business_hours' => 'required|string|max:191'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $contact = Contact::find(1);
        $contact->address = $request->address;
        $contact->mobile_no = $request->mobile_no;
        $contact->telephone_no = $request->telephone_no;
        $contact->email = $request->email;
        $contact->business_hours = $request->business_hours;
        $contact->save();

        return response()->json(['success' => 'Contact successfully updated', 'data' => $contact]);
    }
}
