<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff')->except('show');
    }

    public function index()
    {
        $categories = Category::latest()->get();
        return view('categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $category = Category::create([
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description
        ]);

        return response()->json(['success' => 'Category successfully saved', 'data' => $category]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('categories')->ignore($request->id)
            ]
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $category = Category::findOrFail($request->id);
        $category->name = $request->name;
        $category->slug = str_slug($request->name);
        $category->description = $request->description;
        $category->save();

        return response()->json(['success' => 'Category successfully updated', 'data' => $category]);
    }

    public function destroy($id)
    {
        try {
            Category::destroy($id);
            return response()->json(['success' => 'Category successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }

    public function show($slug)
    {
        if (strcmp($slug, "all") === 0) {
            $category = (object) ['name' => 'All', 'slug' => 'all'];
            $products = Product::latest('updated_at')->paginate(12);
        } else {
            $category = Category::where('slug', $slug)->firstOrFail();
            $products = Product::where('category_id', $category->id)->latest('updated_at')->paginate(12);
        }
        
        return view('categories.show', compact('products', 'category'));
    }
}
