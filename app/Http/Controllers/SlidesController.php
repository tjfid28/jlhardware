<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SlidesController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function index()
    {
        $slides = Slide::latest()->get();
        return view('slides.index', compact('slides'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|mimes:jpeg,jpg,png|dimensions:min_width=2000,min_height=547'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $extension = '.' . $request->image->getClientOriginalExtension();
        $imageName = sha1(time()) . $extension;
        $request->image->move(public_path('images/slides'), $imageName);

        $slide = Slide::create([
            'image' => $imageName
        ]);
        $slide->image = $slide->getImageUrl();

        return response()->json(['success' => 'Slide successfully saved', 'data' => $slide]);
    }

    /*public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|mimes:jpeg,jpg,png|dimensions:min_width=2000,min_height=547'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $imageName = sha1(time());
        $request->image->move(public_path('slides'), $imageName);

        $slide = Slide::find($request->id);

        unlink($slide->image);

        $slide->image = $imageName;
        $slide->save();



        return response()->json(['success' => 'Slide successfully updated', 'data' => $slide]);
    }*/

    public function destroy($id)
    {
        try {
            $slide = Slide::find($id);
            unlink($slide->getImagePath());
            $slide->delete();
            return response()->json(['success' => 'Slide successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }
}
