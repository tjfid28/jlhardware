@extends('layouts.app')

@section('title', "Product - $product->name")

@section('content')
    <section class="sw-margin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--product-details-->
                    <div class="product-details">
                        <div class="col-md-4">
                            <div class="view-product">
                                <img src="{{ $product->getImageUrl() }}" />
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7">
                            <!--/product-information-->
                            <div class="product-information">
                                <h2 class="product">{{ $product->name }}</h2>

                                @if ($product->isOutOfStock())
                                    <p><b class="text-danger">Out of stock</b></p>
                                @else
                                    <br>
                                @endif

                                <p><b>Price: </b> <span class="price">{{ number_format($product->price, 2) }}</span></p>
                                <p><b>Stocks Available: </b> {{ number_format($product->quantity) }}</p>
                                <p><b>Brand: </b> <a href="{{ route('brands.show', $product->brand->slug) }}">{{ $product->brand->name }}</a></p>
                                <p><b>Category: </b> <a href="{{ route('categories.show', $product->category->slug) }}">{{ $product->category->name }}</a></p>
                                <p><b>Subcategory: </b> <a href="{{ route('subcategories.show', $product->subcategory->slug) }}">{{ $product->subcategory->name }}</a></p>
                                <p><b>Description: </b> {{ $product->description }}</p>
                                <p>@include('includes.message')</p>
                                
                                @if (!$product->isOutOfStock())
                                    <p style="margin-top: 15px;">
                                        <form method="POST" action="{{ route('cart.store') }}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $product->id }}">
                                            <button class="btn btn-default add-to-cart" type="submit"><i class="fa fa-shopping-cart"></i>Add to Cart</button>
                                            <a class="btn btn-default" style="margin-top: 0" href="{{ route('categories.show', 'all') }}">Continue Shopping</a>
                                        </form>
                                    </p>
                                @endif
                            </div>
                            <!--/product-information-->
                        </div>
                    </div>
                    <!--/product-details-->
                </div>
            </div>
        </div>
    </section>
@endsection
