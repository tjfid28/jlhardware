$(document).ready(() => {
    //Advanced form with validation
    let form = $('#wizard_with_validation').show();

    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
            $.AdminBSB.input.activate();

            //Set tab width
            let $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            let tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            form.submit();
        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            name: {
                required: true,
                normalizer: value => $.trim(value)
            },
            email: {
                required: true,
                email: true,
                normalizer: value => $.trim(value)
            },
            password: {
                required: true,
                normalizer: value => $.trim(value),
                minlength: 6
            },
            password_confirmation: {
                equalTo: '#password'
            },
            mobile_no: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
                normalizer: value => $.trim(value),
            },
            telephone_no: {
                digits: true,
                minlength: 7,
                maxlength: 7,
                normalizer: value => $.trim(value),
            },
            building_street_info: {
                required: true,
                normalizer: value => $.trim(value)
            },
            barangay: {
                required: true,
                normalizer: value => $.trim(value)
            },
            city: {
                required: true,
                normalizer: value => $.trim(value)
            },
            province: {
                required: true,
                normalizer: value => $.trim(value)
            },
            region: {
                required: true,
                normalizer: value => $.trim(value)
            },
            postal_code: {
                required: true,
                digits: true,
                minlength: 4,
                maxlength: 4,
                normalizer: value => $.trim(value)
            }
        },
        messages: {
            password_confirmation: {
                equalTo: "Password does not match"
            },
            mobile_no : {
                minlength: "Please enter a valid mobile no.",
                maxlength: "Please enter a valid mobile no.",
            },
            telephone_no : {
                minlength: "Please enter a valid telephone no.",
                maxlength: "Please enter a valid telephone no.",
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}
