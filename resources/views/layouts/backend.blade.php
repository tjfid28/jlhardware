<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('includes.backend.header')
<body class="theme-blue">
    <!-- Page Loader -->
    @include('includes.backend.page-loader')
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Top Bar -->
    @include('includes.backend.topbar')
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        @include('includes.backend.sidebar')
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            @yield('content')
        </div>
    </section>
    
    <!-- Scripts -->
    @include('includes.backend.script')
</body>
</html>
