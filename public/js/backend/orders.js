setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

$(document).ready(() => {

    /* Variable declaration - initialization */
    const ordersTable = $(document).find('.ordersTable');
    const viewInformationModal = $(document).find('.viewInformationModal');
    const deleteForm = $(document).find('.deleteForm');
    const updateForm = $(document).find('.updateForm');
    const updateStatusModal = $(document).find('.updateStatusModal');

    const dt = ordersTable.DataTable({
        order: [[5, "desc"]],
        "columnDefs": [
            { "orderable": false, "targets": [0, 3] },
            { "searchable": false, "targets": 0 }
        ]
    });

    viewInformationModal.on('show.bs.modal', e => {
        const items = $.parseJSON(e.relatedTarget.dataset.items);
        const rows = items.map(item => {
            const { quantity } = item;
            const { image, name, brand, subcategory, unit, price } = item.product;
            const imageUrl = `${window.location.origin}/images/products/${image}`;
            const total = parseFloat(price) * parseFloat(quantity);

            return `
                <tr>
                    <td>
                        <a href="${imageUrl}" target="_blank">
                            <img src="${imageUrl}" alt="${imageUrl}" width="48" height="24">
                        </a>
                    </td>
                    <td>${name}</td>
                    <td>${brand.name}</td>
                    <td>${subcategory.category.name}</td>
                    <td>${subcategory.name}</td>
                    <td>${unit ? unit : ''}</td>
                    <td>${parseFloat(price).format(2)}</td>
                    <td>${quantity}</td>
                    <td>${total.format(2)}</td>
                </tr>
            `;
        });

        $('.productsTable').html(rows);
    });

    updateStatusModal.on('show.bs.modal', function (e) {
        const id = $(e.relatedTarget).closest('.btnOrderActions').attr('data-id');
        const status = parseInt(e.relatedTarget.dataset.status);
        const orderNumber = $(`.order-number-${id}`).text();

        switch(status) {
            case 2:
                $('.updateStatusModalTitle').html('Are you sure you want <span class="col-blue">to ship</span> this order?');
                break;
            case 3:
                $('.updateStatusModalTitle').html('Are you sure you want <span class="col-orange">to receive</span> this order?');
                break;
            case 4:
                $('.updateStatusModalTitle').html('Are you sure you want to <span class="col-green">complete</span> this order?');
                break;
            case 5:
                $('.updateStatusModalTitle').html('Are you sure you want to <span class="col-red">cancel</span> this order?');
                break;
        }

        $('.update-id').val(id);
        $('.update-status').val(status);
        $('.update-orderNumber').val(orderNumber);
    });

    updateForm.submit(event => {
        event.preventDefault();

        const _method = updateForm.attr('method');
        const id = $('.update-id').val();

        $.ajax({
            url: `/orders/${id}`,
            type: _method,
            beforeSend: () => {
                $('.btnUpdate').html('<i class="fa fa-spinner fa-spin"></i> Updating...');
            },
            data: updateForm.serialize(),
            success: response => {
                if (response.success) {
                    const { data } = response;
                    const tempRow = dt.row(`.row${id}`).data();
                    tempRow[6].display = data.updated_at;
                    $(`.updated_at-${id}`).attr('data-order', data.updated_at);
                    dt.row(`.row${id}`).data(tempRow).invalidate().draw(false);

                    switch(parseInt(data.status_id)) {
                        case 2:
                            $(`.action-${id}`).html(`
                                <button type="button" class="btn btn-warning btn-sm" data-status="3" data-toggle="modal" data-target=".updateStatusModal">To Receive</button>
                            `);
                            $(`.status-${id}`).html(`
                                <span class="label label-primary">${data.status.name}</span>
                            `);
                            break;
                        case 3:
                            $(`.action-${id}`).html(`
                                <button type="button" class="btn btn-success btn-sm" data-status="4" data-toggle="modal" data-target=".updateStatusModal">Complete</button>
                            `);
                            $(`.status-${id}`).html(`
                                <span class="label label-warning">${data.status.name}</span>
                            `);
                            break;
                        case 4:
                            $(`.action-${id}`).html('');
                            $(`.status-${id}`).html(`
                                <span class="label label-success">${data.status.name}</span>
                            `);
                            break;
                        case 5:
                            $(`.action-${id}`).html('');
                            $(`.status-${id}`).html(`
                                <span class="label label-danger">${data.status.name}</span>
                            `);
                            break;
                    }

                    updateStatusModal.modal('hide');
                    dt.draw(false);

                    iziToast.success({
                        title: 'Success',
                        message: response.success
                    });
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: response.errors ? setErrors(response.errors) : response.error,
                        timeout: false
                    });
                }
            },
            error: response => console.log(response),
            complete: () => $('.btnUpdate').html('UPDATE')
        });
    });

});
