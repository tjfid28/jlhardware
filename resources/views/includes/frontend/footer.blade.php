<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <p class="pull-left jl-footer-text">Copyright &copy; {{ date('Y') }} JOHNLYNTIN HARDWARE. All rights reserved.</p>
            <p class="pull-right jl-footer-text">Developers:
                <span><a href="javascript:void(0)">Blesse Grail B. Berjica</a></span>
                <span> | </span>
                <span><a href="javascript:void(0)">Skye Gabriel C. Lapig</a></span>
                <span> | </span>
                <span><a href="javascript:void(0)">Rose Anne J. Labiano</a></span>
                <span> | </span>
                <span><a href="javascript:void(0)">Cindy H. Sabandon</a></span>
            </p>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- bootstrap-select Js -->
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
