<!--header-->
<header id="header">
    <!--/header_top-->
    <div class="header-middle">
        <div class="container">
            <div class="row jl-vertical-center">
                <div class="col-md-3">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('images/frontend/logo.png') }}" height="64px" width="64px" alt="logo" class=" jl-logo"/>
                        </a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="search_box">
                        <form action="{{ route('products.search') }}" method="GET">                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control selectpicker show-tick" name="brand" data-live-search="true">
                                            <option value="">Brand</option>
                                            @forelse ($brands as $brand)
                                                <option value="{{ $brand->slug }}" {{ Request::get('brand') === $brand->slug ? 'selected' : '' }} data-tokens="{{ $brand->name }}">
                                                    {{ $brand->name }}
                                                </option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control selectpicker show-tick" name="category" data-live-search="true">
                                            <option value="">Category</option>
                                            @forelse ($categories as $category)
                                                <option value="{{ $category->slug }}" {{ Request::get('category') === $category->slug ? 'selected' : '' }} data-tokens="{{ $category->name }}">
                                                    {{ $category->name }}
                                                </option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search by Product Name" name="filter" value="{{ Request::get('filter') ?: '' }}" />
                                    <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--header-bottom-->
    <div class="header-bottom navbar navbar-inverse">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header navbar-default">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    @if (count($categories) > 0)
                                        <li><a href="{{ route('categories.show', 'all') }}">All</a></li>
                                    @endif
                                    @forelse ($categories as $category)
                                        <li><a href="{{ route('categories.show', $category->slug) }}">{{ $category->name }}</a></li>
                                    @empty
                                    @endforelse
                                </ul>
                            </li>
                            <li><a href="{{ route('about') }}">About Us</a></li>
                            <li><a href="{{ route('contacts') }}">Contact Us</a></li>
                            <li><a href="{{ route('terms') }}">Terms &amp; Conditions</a></li>
                            <li><a href="{{ route('faqs') }}">FAQs</a></li>
                            <li><a href="{{ route('cart.index') }}">Cart <span class="badge">{{ Cart::content()->count() }}</span></a></li>
                            @if (Auth::check() && Auth::user()->isCustomer())
                                <li class="dropdown">
                                    <a href="#">{{ Auth::user()->name }}<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li>
                                            <a class="dropdown-item" href="{{ route('profile') }}">My account</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('user.orders') }}">My purchase</a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @else
                                <li><a href="{{ route('login') }}">Login</a></li>
                            @endauth
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header-bottom-->
</header>
<!--/header-->
