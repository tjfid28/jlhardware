<?php

return [
    'critical_level' => 10,
    'fast_moving_count' => 15,
    'minimum_order_amount' => 500.00,
];
