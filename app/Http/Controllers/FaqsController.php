<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class FaqsController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function index()
    {
        $faqs = Faq::latest()->get();
        return view('faqs.index', compact('faqs'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:faqs',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $faq = Faq::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        return response()->json(['success' => 'FAQ successfully saved', 'data' => $faq]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => [
                'required',
                Rule::unique('faqs')->ignore($request->id)
            ],
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $faq = Faq::find($request->id);
        $faq->title = $request->title;
        $faq->description = $request->description;
        $faq->save();

        return response()->json(['success' => 'FAQ successfully updated', 'data' => $faq]);
    }

    public function destroy($id)
    {
        try {
            Faq::destroy($id);
            return response()->json(['success' => 'FAQ successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }
}
