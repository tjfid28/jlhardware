<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            BrandsTableSeeder::class,
            CategoriesTableSeeder::class,
            SubcategoriesTableSeeder::class,
            AboutTableSeeder::class,
            ContactsTableSeeder::class,
            FaqsTableSeeder::class,
            TermsTableSeeder::class,
            SlidesTableSeeder::class,
            ProductsTableSeeder::class,
            StatusTableSeeder::class,
            OrdersTableSeeder::class,
            OrderItemsTableSeeder::class,
            PurchaseOrdersTableSeeder::class,
        ]);
    }
}
