@extends('layouts.backend')

@section('title', 'Suppliers')

@section('styles')
    <!-- JQuery dataTables CSS -->
    <link href="{{ asset('css/backend/datatables.min.css') }}" rel="stylesheet">

    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        SUPPLIERS
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-xs-6">
                            <h2>List of Suppliers</h2>
                        </div>

                        <div class="col-md-6 col-xs-6 text-right">
                            <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".addModal">
                                <i class="material-icons">add</i>
                                <span>Add new supplier</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Actions</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Mobile No.</th>
                                    <th class="text-center">Telephone No.</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Created At</th>
                                    <th class="text-center">Updated At</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse ($suppliers as $supplier)
                                    <tr class="row{{ $supplier->id }}">
                                        <td class="btnActionContainer">
                                            <button type="button" data-id="{{ $supplier->id }}" class="btn btn-success btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".editModal" title="edit">
                                                <i class="material-icons">edit</i>
                                            </button>
                                            &nbsp;
                                            <button type="button" data-id="{{ $supplier->id }}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                        <td class="name-{{ $supplier->id }}">{{ $supplier->name }}</td>
                                        <td class="email-{{ $supplier->id }}">{{ $supplier->email }}</td>
                                        <td class="mobile_no-{{ $supplier->id }}" data-mobile_no="{{ $supplier->profile->mobile_no }}">
                                            {{ $supplier->profile->getFormattedMobileNo() }}
                                        </td>
                                        <td class="telephone_no-{{ $supplier->id }}" data-telephone_no="{{$supplier->profile->telephone_no }}">
                                            {{ $supplier->profile->telephone_no ? $supplier->profile->getFormattedTelephoneNo() : null }}
                                        </td>
                                        <td class="address-{{ $supplier->id }}" data-building_street_info="{{ $supplier->profile->building_street_info }}" data-barangay="{{ $supplier->profile->barangay }}" data-city="{{ $supplier->profile->city }}" data-province="{{ $supplier->profile->province }}" data-region="{{ $supplier->profile->region }}" data-postal_code="{{ $supplier->profile->postal_code }}">
                                            {{ $supplier->profile->getAddress() }}
                                        </td>
                                        <td data-order="{{ $supplier->created_at }}">{{ $supplier->created_at }}</td>
                                        <td class="updated_at-{{ $supplier->id }}" data-order="{{ $supplier->updated_at }}">
                                            {{ $supplier->updated_at }}
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade addModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Add new supplier</h4>
                </div>
                <form class="addForm" action="{{ route('suppliers.store') }}" method="POST">
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-name" name="name">
                                <label class="form-label">Name*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control add-email" name="email">
                                <label class="form-label">Email*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control add-password" name="password">
                                <label class="form-label">Password*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control add-password_confirmation" name="password_confirmation">
                                <label class="form-label">Confirm Password*</label>
                            </div>
                        </div>
                        <b>Mobile No.*</b>
                        <div class="form-group input-group">
                            <span class="input-group-addon">
                                +63
                            </span>
                            <div class="form-line">
                                <input type="number" class="form-control add-mobile_no" placeholder="Ex. 9876543210" name="mobile_no">
                            </div>
                        </div>
                        <b>Telephone No. <i>(optional)</i></b>
                        <div class="form-group input-group">
                            <span class="input-group-addon">
                                (02)
                            </span>
                            <div class="form-line">
                                <input type="number" class="form-control add-telephone_no" placeholder="Ex. 1234567" name="telephone_no">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-building_street_info" name="building_street_info">
                                <label class="form-label">Bldg. / Street / Subd.*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-barangay" name="barangay">
                                <label class="form-label">Barangay*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-city" name="city">
                                <label class="form-label">City*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-province" name="province">
                                <label class="form-label">Province*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-region" name="region">
                                <label class="form-label">Region*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control add-postal_code" name="postal_code">
                                <label class="form-label">Postal code*</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnSave waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Edit supplier</h4>
                </div>
                <form class="editForm" method="PATCH">
                    <div class="modal-body">
                        <!-- Alerts -->
                        <input class="edit-id" type="hidden" name="id">

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-name" name="name">
                                <label class="form-label">Name*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control edit-email" name="email">
                                <label class="form-label">Email*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control edit-password" name="password">
                                <label class="form-label">Password</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control edit-password_confirmation" name="password_confirmation">
                                <label class="form-label">Confirm Password</label>
                            </div>
                        </div>
                        <b>Mobile No.*</b>
                        <div class="form-group input-group">
                            <span class="input-group-addon">
                                +63
                            </span>
                            <div class="form-line">
                                <input type="number" class="form-control edit-mobile_no" placeholder="Ex. 9876543210" name="mobile_no">
                            </div>
                        </div>
                        <b>Telephone No. <i>(optional)</i></b>
                        <div class="form-group input-group">
                            <span class="input-group-addon">
                                (02)
                            </span>
                            <div class="form-line">
                                <input type="number" class="form-control edit-telephone_no" placeholder="Ex. 1234567" name="telephone_no">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-building_street_info" name="building_street_info">
                                <label class="form-label">Bldg. / Street / Subd.*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-barangay" name="barangay">
                                <label class="form-label">Barangay*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-city" name="city">
                                <label class="form-label">City*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-province" name="province">
                                <label class="form-label">Province*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-region" name="region">
                                <label class="form-label">Region*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control edit-postal_code" name="postal_code">
                                <label class="form-label">Postal code*</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnUpdate waves-effect">UPDATE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Delete supplier</h4>
                </div>
                <form class="deleteForm form-horizontal" method="DELETE">
                    <div class="modal-body">
                        <h5>Are you sure you want to delete this data?</h5>
                        <input class="delete-id" type="hidden" name="id">

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Name</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-name" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Email</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-email" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Mobile No.</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-mobile_no" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Telephone No.</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-telephone_no" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Address</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-address" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnDelete waves-effect">DELETE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- JQuery dataTables JS -->
    <script type="text/javascript" src="{{ asset('js/backend/datatables.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/suppliers.js') }}"></script>
@endsection
