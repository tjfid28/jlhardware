@extends('layouts.backend')

@section('title', 'Terms and Conditions')

@section('styles')
    <!-- JQuery dataTables CSS -->
    <link href="{{ asset('css/backend/datatables.min.css') }}" rel="stylesheet">

    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        TERMS AND CONDITIONS
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-xs-6">
                            <h2>List of Terms and Conditions</h2>
                        </div>

                        <div class="col-md-6 col-xs-6 text-right">
                            <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".addModal">
                                <i class="material-icons">add</i>
                                <span>Add new term and condition</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Actions</th>
                                    <th class="text-center">Description</th>
                                    <th class="text-center">Created At</th>
                                    <th class="text-center">Updated At</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse ($terms as $term)
                                    <tr class="row{{ $term->id }}">
                                        <td class="btnActionContainer">
                                            <button type="button" data-id="{{ $term->id }}" class="btn btn-success btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".editModal" title="edit">
                                                <i class="material-icons">edit</i>
                                            </button>
                                            &nbsp;
                                            <button type="button" data-id="{{ $term->id }}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                        <td class="description-{{ $term->id }}">{{ $term->description }}</td>
                                        <td data-order="{{ $term->created_at }}">{{ $term->created_at }}</td>
                                        <td class="updated_at-{{ $term->id }}" data-order="{{ $term->updated_at }}">{{ $term->updated_at }}</td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade addModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Add new term and condition</h4>
                </div>
                <form class="addForm" action="{{ route('terms.store') }}" method="POST">
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize add-description" name="description"></textarea>
                                <label class="form-label">Description*</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnSave waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Edit termn and condition</h4>
                </div>
                <form class="editForm" method="PATCH">
                    <div class="modal-body">
                        <!-- Alerts -->
                        <input class="edit-id" type="hidden" name="id">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize edit-description" name="description"></textarea>
                                <label class="form-label">Description*</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnUpdate waves-effect">UPDATE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Delete term and condition</h4>
                </div>
                <form class="deleteForm form-horizontal" method="DELETE">
                    <div class="modal-body">
                        <h5>Are you sure you want to delete this data?</h5>
                        <input class="delete-id" type="hidden" name="id">

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Description</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <textarea rows="4" class="form-control no-resize delete-description" disabled></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnDelete waves-effect">DELETE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- JQuery dataTables JS -->
    <script type="text/javascript" src="{{ asset('js/backend/datatables.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/terms.js') }}"></script>
@endsection
