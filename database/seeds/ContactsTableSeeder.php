<?php

use App\Contact;
use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
            'address' => 'Miramonte Avenue Soldier Hills III SUBDV, Caloocan, Metro Manila',
            'mobile_no' => '9235737529',
            'email' => 'johnlyntinhardware@gmail.com',
            'business_hours' => 'Monday - Saturday 7:00 AM - 5:00 PM',
        ]);
    }
}
