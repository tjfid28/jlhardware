<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Gloudemans\Shoppingcart\Contracts\Buyable;

class Product extends Model implements Buyable
{
    protected $fillable = ['brand_id', 'category_id', 'subcategory_id', 'supplier_id', 'name', 'slug', 'price', 'quantity', 'description', 'image'];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function supplier()
    {
        return $this->belongsTo('App\User');
    }

    public function orderItem()
    {
        return $this->belongsTo('App\OrderItem');
    }

    public function getImagePath()
    {
        return public_path("images/products/$this->image");
    }

    public function getImageUrl()
    {
        return asset("images/products/$this->image");
    }

    public function scopeAvailable($query)
    {
        return $query->where('quantity', '>', 0);
    }

    public function scopeSearch($query, $searchTerm)
    {
        return $query->where('name', 'like', "%$searchTerm%");
    }

    public function isOutOfStock()
    {
        return $this->quantity == 0;
    }

    public function isCritical()
    {
        return $this->quantity <= config('env.critical_level');
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->name;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }
}
