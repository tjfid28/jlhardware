<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function fastMovingItems()
    {
        $products = Product::where('order_count', '>=', config('env.fast_moving_count'))->latest('updated_at')->get();
        $title = 'Fast Moving Items';
        return view('inventory.index', compact('products', 'title'));
    }

    public function slowMovingItems()
    {
        $products = Product::whereBetween('order_count', [1, config('env.fast_moving_count') - 1])->latest('updated_at')->get();
        $title = 'Slow Moving Items';
        return view('inventory.index', compact('products', 'title'));
    }

    public function criticalLevel()
    {
        $products = Product::where('quantity', '<=', config('env.critical_level'))->latest('updated_at')->get();
        $title = 'Critical Level';
        return view('inventory.index', compact('products', 'title'));
    }
}
