<?php

use App\Faq;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Faq::insert([
            [
                'title' => 'HOW CAN I CHANGE MY SHIPPING ADDRESS?',
                'description' => 'By default, the last used shipping address will be saved into to your Sample Store account. When you are checking out your order, the default shipping address will be displayed and you have the option to amend it if you need to.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'HOW DO I ACTIVATE MY ACCOUNT?',
                'description' => 'The instructions to activate your account will be sent to your email once you have submitted the registration form. If you did not receive this email, your email service provider mailing software may be blocking it. You can try checking your junk / spam folder or contact us at help@jlhardware.com',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'WHAT DO YOU MEAN BY POINTS? HOW DO I EARN IT?',
                'description' => 'Because you are important to us, we want to know what you think about the products. As an added value, every time you rate the products you earn points which go straight to your account. 1 point are added to your account for every review that you give. You will need those points in order to redeem the sample products. So keep rating the products to keep earning points!',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'WHY IS THERE A CHECKOUT LIMIT? / WHAT ARE ALL THE CHECKOUT LIMITS?',
                'description' => 'Sample Store is a popular spot and gets lots of shoppers at a time. These limits are in place to make sure everyone has a good time trying and purchasing their products. So... - Each member is entitled to only one (1) sample order every day. - Each member is entitled to one (1) bundle of sample for each product. - Your account must have sufficient points before you can checkout the sample products. - Kindly clear all pending payments before another checkout.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'HOW CAN I TRACK MY ORDERS & PAYMENT?',
                'description' => 'After logging into your account, the status of your checkout history can be found under Order History. For orders via registered postage, a tracking number (article tracking number) will be given to you after the receipt given from Singapore Post Limited (SingPost).',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'HOW DO I CANCEL MY ORDERS BEFORE I MAKE A PAYMENT?',
                'description' => 'After logging into your account, go to your Shopping Cart. Here, you will be able to make payment or cancel your order. Note: We cannot give refunds once payment is verified.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'HOW LONG WILL IT TAKE FOR MY ORDER TO ARRIVE AFTER I MAKE PAYMENT?',
                'description' => 'Members who ship their orders within Singapore should expect to receive their orders within five (5) to ten (10) working days upon payment verification depending on the volume of orders received. If you experience delays in receiving your order, contact us immediately and we will help to confirm the status of your order.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'WHAT IS THE ACCUMULATED DELIVERY FEE FOR? HOW MUCH IS THE HANDLING FEE?',
                'description' => 'The flat-rate handling fee is S$5.99 and it is only applicable to normal samples. For free samples, they are fully paid for and there are no additional charges to deliver the free samples. Handling fee covers the delivery, material, labour and logistics cost to support the sampling service. You can redeem up to 4 different samples in each checkout and it is likely that you will find samples bundle with larger supply (up to 1-week supply) on Samplestore.com.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'HOW DO YOU SHIP MY ORDERS?',
                'description' => 'All your orders are sent via Philippines Post Limited (SingPost).',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'title' => 'HOW DO I MAKE PAYMENTS USING PAYPAL? HOW DOES IT WORK?',
                'description' => "Paypal is the easiest way to make payments online. While checking out your order, you will be redirected to the Paypal website. Be sure to fill in correct details for fast & hassle-free payment processing. After a successful Paypal payment, a payment advice will be automatically generated to Samplestore.com system for your order. It's fast, easy & secure.",
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
