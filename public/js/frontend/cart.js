$(document).ready(() => {
    /* Tooltip */
    $('body').tooltip({
        selector: '[data-tooltip="tooltip"]',
        trigger : 'hover',
        container: 'body'
    });

    $(".spinner")
        .spinner('delay', 0) //delay in ms
        .spinner('changed', function(e, newVal, oldVal) {
            // trigger lazed, depend on delay option.
            const id = $(this).closest('tr').data('id');
            const rowId = $(this).closest('tr').data('rowid');

            $.ajax({
                url: `/cart/${id}`,
                type: 'PATCH',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: { id, rowId, qty: newVal },
                success: response => {
                    if (response.success) {
                        $(this).closest('tr').find('.itotal').text(response.total);
                        $('.subtotal').text(response.subtotal);
                        $('.vat').text(response.vat);
                        $('.amount-due').text(response.subtotal);

                        if (parseFloat(response.subtotalParse) >= parseFloat($('.minimum-order-amount').val())) {
                            $('.btn-check-out').removeClass('hidden');
                        } else {
                            if (!$('.btn-check-out').hasClass('hidden')) {
                                $('.btn-check-out').addClass('hidden');
                            }
                        }
                    } else {
                        $(this)[0].value = response.qty;
                        iziToast.error({
                            title: 'Error',
                            message: response.error,
                            position: 'topRight'
                        });
                    }
                },
                error: response => console.log(response),
            });
        })
        .spinner('changing', function(e, newVal, oldVal) {
            // trigger immediately
        });

    $('.deleteModal').on('show.bs.modal', e => {
        const rowId = e.relatedTarget.dataset.id;
        const name = e.relatedTarget.dataset.name;

        $('.deleteForm').attr('action', `/cart/${rowId}`);
        $('.delete-name').val(name);
    });
});
