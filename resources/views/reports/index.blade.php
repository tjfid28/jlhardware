@extends('layouts.backend')

@section('title', 'REPORT')

@section('styles')
    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- bootstrap-select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">

    <!-- daterangepicker CSS -->
    <link href="{{ asset('css/backend/daterangepicker.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        REPORTS
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-12"><h2>Generate report</h2></div>
                    </div>
                </div>

                <div class="body">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <a type="button" class="report-image" data-toggle="modal" data-target=".inventory-modal">
                                    <img src="{{ asset('images/backend/inventory-report.png') }}">
                                </a>
                                <div class="caption">
                                    <h3>Inventory Report</h3>
                                    <p>
                                        Generate <b>inventory</b> reports here
                                    </p>
                                    <p>
                                        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".inventory-modal">GENERATE</button>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <a type="button" class="report-image" data-toggle="modal" data-target=".orders-modal">
                                    <img src="{{ asset('images/backend/orders-report.png') }}">
                                </a>
                                <div class="caption">
                                    <h3>Orders Report</h3>
                                    <p>
                                        Generate <b>orders</b> reports here
                                    </p>
                                    <p>
                                        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".orders-modal">GENERATE</button>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <a type="button" class="report-image" data-toggle="modal" data-target=".sales-modal">
                                    <img src="{{ asset('images/backend/sales-report.png') }}">
                                </a>
                                <div class="caption">
                                    <h3>Sales Report</h3>
                                    <p>
                                        Generate <b>sales</b> reports here
                                    </p>
                                    <p>
                                        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".sales-modal">GENERATE</button>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <a type="button" class="report-image" data-toggle="modal" data-target=".purchase-orders-modal">
                                    <img src="{{ asset('images/backend/purchase-orders-report.png') }}">
                                </a>
                                <div class="caption">
                                    <h3>Purchase Orders Report</h3>
                                    <p>
                                        Generate <b>purchase orders</b> reports here
                                    </p>
                                    <p>
                                        <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".purchase-orders-modal">GENERATE</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade inventory-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Inventory Report</h4>
                </div>
                <form class="inventory-form" action="{{ route('reports.inventory') }}" method="GET">
                    <div class="modal-body">
                        <label for="type">Type</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control inventory-type selectpicker show-tick" name="type">
                                    <option value="">-- Please select a type --</option>
                                    <option value="all">All</option>
                                    <option value="critical">Critical</option>
                                    <option value="fast-moving">Fast moving</option>
                                    <option value="slow-moving">Slow moving</option>
                                </select>
                            </div>
                        </div>

                        <label for="daterange">Date</label>
                        <div class="form-group inventory-reportrange reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>

                        <div class="form-group">
                            <label>File type</label>
                            <input name="file-type" type="radio" class="with-gap" id="pdf_1" value="pdf" checked />
                            <label for="pdf_1">PDF</label>
                            <input name="file-type" type="radio" class="with-gap" id="excel_1" value="excel" />
                            <label for="excel_1">Excel</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                        <button type="button" class="btn waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade orders-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Orders Report</h4>
                </div>
                <form class="orders-form" action="{{ route('reports.orders') }}" method="GET">
                    <div class="modal-body">
                        <label for="type">Status</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control order-status selectpicker show-tick">
                                    <option value="">-- Please select a status --</option>
                                    <option value="all">All</option>
                                    <option value="pending">Pending</option>
                                    <option value="to-ship">To Ship</option>
                                    <option value="to-receive">To Receive</option>
                                    <option value="cancelled">Cancelled</option>
                                </select>
                            </div>
                        </div>

                        <label for="daterange">Date</label>
                        <div class="form-group orders-reportrange reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>

                        <div class="form-group">
                            <label>File type</label>
                            <input name="file-type" type="radio" class="with-gap" id="pdf_2" value="pdf" checked />
                            <label for="pdf_2">PDF</label>
                            <input name="file-type" type="radio" class="with-gap" id="excel_2" value="excel" />
                            <label for="excel_2">Excel</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                        <button type="button" class="btn waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade sales-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Sales Report</h4>
                </div>
                <form class="sales-form" action="{{ route('reports.sales') }}" method="GET">
                    <div class="modal-body">
                        <label for="daterange">Date</label>
                        <div class="form-group sales-reportrange reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>

                        <div class="form-group">
                            <label>File type</label>
                            <input name="file-type" type="radio" class="with-gap" id="pdf_3" value="pdf" checked />
                            <label for="pdf_3">PDF</label>
                            <input name="file-type" type="radio" class="with-gap" id="excel_3" value="excel" />
                            <label for="excel_3">Excel</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                        <button type="button" class="btn waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade purchase-orders-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Purchase Orders Report</h4>
                </div>
                <form class="purchase-orders-form" action="{{ route('reports.purchaseOrders') }}" method="GET">
                    <div class="modal-body">
                        <label for="type">Status</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control purchase-order-status selectpicker show-tick">
                                    <option value="">-- Please select a status --</option>
                                    <option value="all">All</option>
                                    <option value="pending">Pending</option>
                                    <option value="completed">Completed</option>
                                    <option value="cancelled">Cancelled</option>
                                    <option value="received">Received</option>
                                </select>
                            </div>
                        </div>

                        <label for="daterange">Date</label>
                        <div class="form-group purchase-orders-reportrange reportrange">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                        <button type="button" class="btn waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- bootstrap-select Js -->
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>

    <!-- daterangepicker Js -->
    <script src="{{ asset('js/backend/moment.min.js') }}"></script>
    <script src="{{ asset('js/backend/daterangepicker.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/reports.js') }}"></script>
@endsection
