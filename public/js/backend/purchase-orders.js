$(document).ready(() => {

    const form = $(document).find('.form');
    const modalCancel = $(document).find('.modal-cancel');
    const modalReceive = $(document).find('.modal-receive');
    const modalDelete = $(document).find('.modal-delete');
    const modalComplete = $(document).find('.modal-complete');

    try {
        $('textarea').froalaEditor();
    } catch (e) {}

    try {
        form.validate({
            ignore: [],
            rules: {
                supplier_id: {
                    required: true
                },
                description: {
                    required: true,
                    normalizer: value => $.trim(value)
                }
            },
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            }
        });
    } catch (e) {}

    modalCancel.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const ponumber = e.relatedTarget.dataset.ponumber;

        $('.cancel-modal-title').html(`Are you sure you want to CANCEL <i>${ponumber}</i> ?`);
        $('.form-cancel').attr('action', `/purchase-orders/cancel/${id}`);
    });

    modalReceive.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const ponumber = e.relatedTarget.dataset.ponumber;

        $('.receive-modal-title').html(`Are you sure you want to RECEIVE <i>${ponumber}</i> ?`);
        $('.form-receive').attr('action', `/purchase-orders/receive/${id}`);
    });

    modalDelete.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const ponumber = e.relatedTarget.dataset.ponumber;

        $('.delete-modal-title').html(`Are you sure you want to DELETE <i>${ponumber}</i> ?`);
        $('.form-delete').attr('action', `/purchase-orders/${id}`);
    });

    modalComplete.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const ponumber = e.relatedTarget.dataset.ponumber;

        $('.complete-modal-title').html(`Are you sure you want to COMPLETE <i>${ponumber}</i> ?`);
        $('.form-complete').attr('action', `/purchase-orders/complete/${id}`);
    });

});
