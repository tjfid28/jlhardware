<?php

use App\About;
use Illuminate\Database\Seeder;

class AboutTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        About::create([
            'content' => '<p>Johnlyntin Hardware, started in March 28, 2009 with Mrs. Juliet Sumabat Dollar as the proprietor. The owner has only a mini Sari-Sari Store for about 15 years ago. And when they had already the investment to expand their business, they have chosen to upgrade their business as construction hardware. Until now, they still in good sales and planning to expand more. Always offer bulk orders with freebies like brick trowel, rectangular trowel, shovel, and panel saw and etc. The payment is cash on delivery and cash.</p><p>Johnlyntin, is the name of the hardware which came from the 3 combined names of their children, John, Angelyn, and Christine. The owner submitted her hardware to her first son but she is still the one who managing their store and she named her son as the owner of the hardware. Johnlyntin Hardware core products can be summarized under the home improvement merchandise, its 8 main departments are: Paints and Sundries, Electrical, Tools, Plumbing, Building Materials, Concrete, Hardware Supplies, & Pets.</p><p>Johnlyntin Hardware is providing well-trained sales personnel ready to answer questions and professionally handle requirements.</p>'
        ]);
    }
}
