<?php

namespace App\Http\Controllers;

use App\About;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function index()
    {
        $orders = Order::all()->groupBy('status_id');
        $inventory = Product::all();
        $fastMoving = $inventory->where('order_count', '>=', config('env.fast_moving_count'));
        $slowMoving = $inventory
            ->where('order_count', '>=', 1)
            ->where('order_count', '<=', config('env.fast_moving_count') - 1);
        $criticalLevel = $inventory->where('quantity', '<=', config('env.critical_level'));

        $orderCount = [
            'pending' => $orders[1]->count(),
            'to-ship' => $orders[2]->count(),
            'to-receive' => $orders[3]->count(),
            'completed' => $orders[4]->count(),
            'cancelled' => $orders[5]->count()
        ];

        $inventoryCount = [
            'all' => $inventory->count(),
            'fast-moving' => $fastMoving->count(),
            'slow-moving' => $slowMoving->count(),
            'critical-level' => $criticalLevel->count()
        ];

        return view('backend.dashboard', compact('orderCount', 'inventoryCount'));
    }
}
