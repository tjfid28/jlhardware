<!-- Jquery Core Js -->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{ asset('js/backend/jquery.slimscroll.min.js') }}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ asset('js/backend/waves.min.js') }}"></script>

<!-- Custom Js -->
<script src="{{ asset('js/backend/admin.js') }}"></script>

<!-- Additional script -->
@yield('scripts')
