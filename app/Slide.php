<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['image'];

    public function getImagePath()
    {
        return public_path("images/slides/$this->image");
    }

    public function getImageUrl()
    {
        return asset("images/slides/$this->image");
    }
}
