<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\Status;
use App\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->except(['verify', 'orders', 'cancelOrder', 'cancelOrderItem']);
        $this->middleware('customer')->only(['orders', 'cancelOrder', 'cancelOrderItem']);
    }

    public function index()
    {
        $users = User::whereNotIn('role', ['admin', 'supplier'])->latest()->get();
        return view('users.index', compact('users'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:191|unique:users',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => 'staff'
        ]);

        return response()->json(['success' => 'User successfully saved', 'data' => $user]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required','string','max:191',
                Rule::unique('users')->ignore($request->id)
            ],
            'email' => [
                'required','string','email','max:191',
                Rule::unique('users')->ignore($request->id)
            ],
            'password' => 'nullable|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        if (!is_null($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return response()->json(['success' => 'User successfully updated', 'data' => $user]);
    }

    public function destroy($id)
    {
        try {
            User::destroy($id);
            return response()->json(['success' => 'User successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }

    public function verify($token)
    {
        User::where('token', $token)->firstOrFail()
            ->update(['token' => null]);

        return redirect('login')
            ->with('success', 'Account verified!');
    }

    public function orders()
    {
        $status = Status::where('id', '!=', 6)->get();
        $user = auth()->user();
        $orders = Order::where('user_id', $user->id)->get()->sortByDesc('created_at');

        return view('users.orders', compact('status', 'orders'));
    }

    public function cancelOrder(Request $request)
    {
        $order = Order::findOrFail($request->id);
        $valid = true;

        if ($order->status->isPending()) {
            $responseMessage = "Order has been <b>Cancelled</b>";

            foreach ($order->orderItems as $key => $item) {
                $product = $item->product;
                $product->quantity += $item->quantity;
                $product->save();
            }
        } else {
            $valid = false;
            $responseMessage = "Order could not be <b>Cancelled</b>, order status might have been changed";
        }

        if (!$valid) {
            session()->flash('warning', $responseMessage);
        } else {
            $order->status_id = 5;
            $order->save();
            session()->flash('success', $responseMessage);
        }

        return redirect()->back();
    }

    public function cancelOrderItem(Request $request)
    {
        $orderItem = OrderItem::findOrFail($request->id);
        $valid = true;

        if ($orderItem->order->status->isPending()) {
            $itemAmount = $orderItem->quantity * $orderItem->product->price;
            $minimumOrderAmount = config('env.minimum_order_amount');

            if (($orderItem->order->getTotalAmount() - $itemAmount) < $minimumOrderAmount) {
                $valid = false;
                $responseMessage = "Order Item: <b>{$orderItem->product->name}</b> could not be <b>Cancelled</b>. Minimum amount of <b>${minimumOrderAmount}</b> is required.";
            } else {
                $product = $orderItem->product;
                $product->quantity += $orderItem->quantity;
                $product->save();

                $responseMessage = "Order Item: <b>{$orderItem->product->name}</b> has been <b>Cancelled</b>";
                $orderItem->delete();
            }
        } else {
            $valid = false;
            $responseMessage = "Order Item: <b>{$orderItem->product->name}</b> could not be <b>Cancelled</b>, Order status might have been changed.";
        }

        if (!$valid) {
            session()->flash('warning', $responseMessage);
        } else {
            session()->flash('success', $responseMessage);
        }

        return redirect()->back();
    }
}
