<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{ asset('css/backend/reports.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            <h1 class="text-center">{{ $title }}</h1>
            <div class="col-md-12 text-center">
                <h3>ANTHONYX GENERAL MERCHANDISE</h3>
                <h4>B22 Lot 2, Swimming Pool St. Cor. Maligaya St. Caloocan City</h4>
                <h4>John Anthony D. Sumabat</h4>
                <h4>09235737529</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 filters">
                {!! $dateRange !!}
            </div>
            <div class="col-sm-6 filters">
                {!! $filter !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                        <th>No.</th>
                        @foreach ($headers as $key => $header)
                            <th class="{{ $key < 5 ? '' : 'text-right' }}">{{ $header }}</th>
                        @endforeach
                    </thead>
                    <tbody>
                        @if (strcmp($type, 'inventory') === 0)
                            @foreach ($data as $k => $v)
                                <tr>
                                    <td>{{ $k + 1 }}</td>
                                    <td>{{ $v->name }}</td>
                                    <td>{{ $v->brand->name }}</td>
                                    <td>{{ $v->category->name }}</td>
                                    <td>{{ $v->subcategory->name }}</td>
                                    <td>{{ $v->supplier->name }}</td>
                                    <td class="text-right">{{ number_format($v->price, 2) }}</td>
                                    <td class="text-right">{{ $v->quantity }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
