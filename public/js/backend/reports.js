$(document).ready(() => {

    /* Variable declaration - initialization */
    const inventoryForm = $(document).find('.inventory-form');
    const ordersForm = $(document).find('.orders-form');
    const salesForm = $(document).find('.sales-form');
    const purchaseOrdersForm = $(document).find('.purchase-orders-form');
    const inventoryReportRange = $(document).find('.inventory-reportrange');
    const ordersReportRange = $(document).find('.orders-reportrange');
    const salesReportRange = $(document).find('.sales-reportrange');
    const purchaseOrdersReportRange = $(document).find('.purchase-orders-reportrange');

    /* Date Range Picker - START */
    const start = moment();
    const end = moment();

    const inventoryCB = (start, end) => {
        $('.inventory-reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    const ordersCB = (start, end) => {
        $('.orders-reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    const salesCB = (start, end) => {
        $('.sales-reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    const purchaseOrdersCB = (start, end) => {
        $('.purchase-orders-reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    inventoryReportRange.daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'This Week': [moment().startOf('week'), moment().endOf('week')],
           'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, inventoryCB);

    ordersReportRange.daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'This Week': [moment().startOf('week'), moment().endOf('week')],
           'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, ordersCB);

    salesReportRange.daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'This Week': [moment().startOf('week'), moment().endOf('week')],
           'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, salesCB);

    purchaseOrdersReportRange.daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'This Week': [moment().startOf('week'), moment().endOf('week')],
           'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, purchaseOrdersCB);

    inventoryCB(start, end);
    ordersCB(start, end);
    salesCB(start, end);
    purchaseOrdersCB(start, end);
    /* Date Range Picker - END */

    inventoryForm.submit(function(event) {
        event.preventDefault();

        const type = $('.inventory-type').selectpicker('val') ? $('.inventory-type').selectpicker('val') : 'all';
        const startDate = $(this).find('.reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        const endDate = $(this).find('.reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        const fileType = $(this).find("input[name='file-type']:checked").val();

        window.location.href = `${inventoryForm.attr('action')}?type=${type}&startDate=${startDate}&endDate=${endDate}&fileType=${fileType}`;
    });

    ordersForm.submit(function(event) {
        event.preventDefault();

        const status = $('.order-status').selectpicker('val') ? $('.order-status').selectpicker('val') : 'all';
        const startDate = $(this).find('.reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        const endDate = $(this).find('.reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        const fileType = $(this).find("input[name='file-type']:checked").val();

        window.location.href = `${ordersForm.attr('action')}?status=${status}&startDate=${startDate}&endDate=${endDate}&fileType=${fileType}`;
    });

    salesForm.submit(function(event) {
        event.preventDefault();

        const startDate = $(this).find('.reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        const endDate = $(this).find('.reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        const fileType = $(this).find("input[name='file-type']:checked").val();

        window.location.href = `${salesForm.attr('action')}?startDate=${startDate}&endDate=${endDate}&fileType=${fileType}`;
    });

    purchaseOrdersForm.submit(function(event) {
        event.preventDefault();

        const status = $('.purchase-order-status').selectpicker('val') ? $('.purchase-order-status').selectpicker('val') : 'all';
        const startDate = $(this).find('.reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        const endDate = $(this).find('.reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD');

        window.location.href = `${purchaseOrdersForm.attr('action')}?status=${status}&startDate=${startDate}&endDate=${endDate}`;
    });

});
