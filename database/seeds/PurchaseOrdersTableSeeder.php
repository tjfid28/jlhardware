<?php

use Carbon\Carbon;
use App\PurchaseOrder;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PurchaseOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        PurchaseOrder::insert([
            [
                'supplier_id' => 4,
                'status_id' => 1,
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'supplier_id' => 4,
                'status_id' => 4,
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'supplier_id' => 4,
                'status_id' => 5,
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'supplier_id' => 4,
                'status_id' => 6,
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
