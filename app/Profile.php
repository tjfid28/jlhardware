<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id', 'mobile_no', 'telephone_no', 'building_street_info', 'barangay', 'city', 'province', 'region', 'postal_code'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getFormattedMobileNo()
    {
        return "+63$this->mobile_no";
    }

    public function getFormattedTelephoneNo()
    {
        return "+02$this->telephone_no";
    }

    public function getAddress()
    {
        return "$this->building_street_info, $this->barangay, $this->city, $this->province, $this->region, $this->postal_code";
    }
}
