@extends('layouts.backend')

@section('title', 'Purchase Orders')

@section('content')
    @component('components.backend.block-header')
        PURCHASE ORDERS
    @endcomponent

    @include('includes.message')

    @if (Auth::user()->isAdmin())
        <div class="row p-b-10">
            <div class="col-md-12 text-right">
                <a href="{{ route('purchase-orders.create') }}" class="btn btn-primary waves-effect">
                    <i class="material-icons">add</i>
                    <span>Add new purchase order</span>
                </a>
            </div>
        </div>
    @endif

    <div class="row">
        @forelse ($purchaseOrders as $po)
            @php
                $bg =  $po->status->isPending() ? 'bg-grey' : ($po->status->isCancelled() ? 'bg-red' : ($po->status->isCompleted() ? 'bg-green' : 'bg-cyan'));
            @endphp
            <div class="col-md-12">
                <div class="card">
                    <div class="header {{ $bg }}">
                        <h2>{{ $po->getPurchaseOrderNo() }} <small>{{ $po->status->name }}</small></h2>
                        <ul class="header-dropdown m-r-0">
                            @if(auth()->user()->isAdmin())
                                @if ($po->status->isPending())
                                    <li data-tooltip="tooltip" data-placement="top" title="edit">
                                        <a href="{{ route('purchase-orders.edit', $po->id) }}">
                                            <i class="material-icons">edit</i>
                                        </a>
                                    </li>
                                    <li data-tooltip="tooltip" data-placement="top" title="cancel">
                                        <a href="#" data-id="{{ $po->id }}" data-ponumber="{{ $po->getPurchaseOrderNo() }}" data-toggle="modal" data-target=".modal-cancel">
                                            <i class="material-icons">cancel</i>
                                        </a>
                                    </li>
                                @elseif ($po->status->isCancelled() || $po->status->isReceived())
                                    <li data-tooltip="tooltip" data-placement="top" title="delete">
                                        <a href="#" data-id="{{ $po->id }}" data-ponumber="{{ $po->getPurchaseOrderNo() }}" data-toggle="modal" data-target=".modal-delete">
                                            <i class="material-icons">delete_forever</i>
                                        </a>
                                    </li>
                                @elseif ($po->status->isCompleted())
                                    <li data-tooltip="tooltip" data-placement="top" title="receive">
                                        <a href="#" data-id="{{ $po->id }}" data-ponumber="{{ $po->getPurchaseOrderNo() }}" data-toggle="modal" data-target=".modal-receive">
                                            <i class="material-icons">assignment_return</i>
                                        </a>
                                    </li>
                                @endif
                            @else
                                @if ($po->status->isPending())
                                    <li data-tooltip="tooltip" data-placement="top" title="complete">
                                        <a href="#" data-id="{{ $po->id }}" data-ponumber="{{ $po->getPurchaseOrderNo() }}" data-toggle="modal" data-target=".modal-complete">
                                            <i class="material-icons">done</i>
                                        </a>
                                    </li>
                                @endif
                            @endauth
                        </ul>
                    </div>

                    <div class="body">
                        @if (Auth::user()->isAdmin())
                            <h4>Supplier: {{ $po->supplier->name }}</h4>
                        @endif
                        {!! $po->description !!}
                    </div>
                </div>
            </div>
        @empty
            <div class="col-md-12">
                <div class="card">
                    <div class="body text-center">
                        <strong>No Purchase Orders found</strong>
                    </div>
                </div>
            </div>
        @endforelse
    </div>

    {{ $purchaseOrders->links() }}
@endsection

<div class="modal fade modal-cancel" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title cancel-modal-title" id="defaultModalLabel"></h4>
            </div>
            <form action="" method="POST" class="form-cancel">
                @csrf
                @method('PATCH')
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">YES</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">NO</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade modal-receive" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title receive-modal-title" id="defaultModalLabel"></h4>
            </div>
            <form action="" method="POST" class="form-receive">
                @csrf
                @method('PATCH')
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">RECEIVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade modal-complete" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title complete-modal-title" id="defaultModalLabel"></h4>
            </div>
            <form action="" method="POST" class="form-complete">
                @csrf
                @method('PATCH')
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">COMPLETE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade modal-delete" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title delete-modal-title" id="defaultModalLabel"></h4>
            </div>
            <form action="" method="POST" class="form-delete">
                @csrf
                @method('DELETE')
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">DELETE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('scripts')
    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/purchase-orders.js') }}"></script>
@endsection
