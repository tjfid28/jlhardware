<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubcategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff')->except('show');
    }

    public function index()
    {
        $subcategories = Subcategory::latest()->get();
        $categories = Category::all()->sortBy('name');
        return view('subcategories.index', compact('subcategories', 'categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => 'required|unique:subcategories,name,NULL,id,category_id,'.$request->category_id
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $subcategory = Subcategory::create([
            'category_id' => $request->category_id,
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description
        ]);
        $subcategory = $subcategory->load('category');

        return response()->json(['success' => 'Subcategory successfully saved', 'data' => $subcategory]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'name' => "required|unique:subcategories,name,$request->id,id,category_id,$request->category_id"
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $subcategory = Subcategory::find($request->id);
        $subcategory->category_id = $request->category_id;
        $subcategory->name = $request->name;
        $subcategory->slug = str_slug($request->name);
        $subcategory->description = $request->description;
        $subcategory->save();
        $subcategory = $subcategory->load('category');

        return response()->json(['success' => 'Subcategory successfully updated', 'data' => $subcategory]);
    }

    public function destroy($id)
    {
        try {
            Subcategory::destroy($id);
            return response()->json(['success' => 'Subcategory successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }

    public function show($slug)
    {
        if (strcmp($slug, "all") === 0) {
            $subcategory = (object) ['name' => 'All', 'slug' => 'all'];
            $products = Product::latest('updated_at')->paginate(12);
        } else {
            $subcategory = Subcategory::where('slug', $slug)->firstOrFail();
            $products = Product::where('subcategory_id', $subcategory->id)->latest('updated_at')->paginate(12);
        }
        
        return view('subcategories.show', compact('products', 'subcategory'));
    }
}
