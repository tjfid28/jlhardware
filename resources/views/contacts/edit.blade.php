@extends('layouts.backend')

@section('title', 'Contact')

@section('styles')
    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        CONTACT
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <h2>Update contact</h2>
                </div>

                <div class="body">
                    <form class="editForm" action="{{ route('contacts.update') }}" method="PATCH">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize edit-address" name="address">{{ $contact->address }}</textarea>
                                <label class="form-label">Address*</label>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-md-6 col-xs-12">
                                <b>Mobile No.*</b>
                                <div class="form-group masked-input input-group">
                                    <span class="input-group-addon">
                                        +63
                                    </span>
                                    <div class="form-line">
                                        <input type="number" class="form-control edit-mobile_no" placeholder="Ex. 9876543210" name="mobile_no" value="{{ $contact->mobile_no }}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <b>Telephone No. <i>(optional)</i></b>
                                <div class="form-group masked-input input-group">
                                    <span class="input-group-addon">
                                        (02)
                                    </span>
                                    <div class="form-line">
                                        <input type="number" class="form-control edit-telephone_no" placeholder="Ex. 1234567" name="telephone_no" value="{{ $contact->telephone_no }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control edit-email" name="email" value="{{ $contact->email }}">
                                <label class="form-label">Email*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-business_hours" name="business_hours" value="{{ $contact->business_hours }}">
                                <label class="form-label">Business Hours*</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-lg btn-success btnUpdate waves-effect">UPDATE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/contacts.js') }}"></script>
@endsection
