@extends('layouts.backend')

@section('title', 'View Orders')

@section('styles')
    <!-- JQuery dataTables CSS -->
    <link href="{{ asset('css/backend/datatables.min.css') }}" rel="stylesheet">

    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">

    <style type="text/css">
        .ordersTable tr td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('content')
    @component('components.backend.block-header')
        VIEW ORDERS
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <h2>List of Orders</h2>
                        </div>
                    </div>
                </div>

                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover ordersTable">
                            <thead>
                                <tr>
                                    <th class="text-center">Actions</th>
                                    <th class="text-center">Order #</th>
                                    <th class="text-center">Customer</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Created At</th>
                                    <th class="text-center">Updated At</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse ($orders as $order)
                                    <tr class="row{{ $order->id }}">
                                        <td class="btnOrderActions btn-{{ $order->id }}" data-id="{{ $order->id }}">
                                            <button type="button" data-items="{{ $order->orderItems }}" class="btn btn-info btn-sm" data-toggle="modal" data-target=".viewInformationModal">
                                                View
                                            </button>
                                            
                                            <span class="action-{{ $order->id }}">
                                            @if ($order->status->isPending())
                                                <button type="button" class="btn btn-primary btn-sm" data-status="2" data-toggle="modal" data-target=".updateStatusModal">To Ship</button>
                                                <button type="button" class="btn btn-danger btn-sm" data-status="5" data-toggle="modal" data-target=".updateStatusModal">Cancel</button>
                                            @elseif ($order->status->isToShip())
                                                <button type="button" class="btn btn-warning btn-sm" data-status="3" data-toggle="modal" data-target=".updateStatusModal">To Receive</button>
                                            @elseif ($order->status->isToReceive())
                                                <button type="button" class="btn btn-success btn-sm" data-status="4" data-toggle="modal" data-target=".updateStatusModal">Complete</button>
                                            @endif
                                            </span>
                                        </td>
                                        <td class="order-number-{{ $order->id }}">{{ $order->getOrderNumber() }}</td>
                                        <td>{{ $order->user->isAdmin() || $order->user->isStaff() ? $order->name : $order->user->name }}</td>
                                        <td>{{  $order->user->isAdmin() || $order->user->isStaff() ? $order->address : $order->user->profile->getAddress() }}</td>
                                        <td class="status-{{ $order->id }}">
                                            <span class="label {{ $order->status->getLabel() }}">
                                                {{ $order->status->name }}
                                            </span>
                                        </td>
                                        <td data-order="{{ $order->created_at }}">{{ $order->created_at }}</td>
                                        <td class="updated_at-{{ $order->id }}" data-order="{{ $order->updated_at }}">{{ $order->updated_at }}</td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade viewInformationModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">List of Products</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Brand</th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Subcategory</th>
                                    <th class="text-center">Unit</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody class="productsTable text-center">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade updateStatusModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="PATCH" class="updateForm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title updateStatusModalTitle" id="defaultModalLabel">List of Products</h4>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" class="update-id" name="id" value="">
                        <input type="hidden" class="update-status" name="status" value="">

                        <label>Order #</label>
                        <div class="form-line">
                            <input class="form-control update-orderNumber" type="text" name="update-order-no" value="" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnUpdate waves-effect">Yes</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- JQuery dataTables JS -->
    <script type="text/javascript" src="{{ asset('js/backend/datatables.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/orders.js') }}"></script>
@endsection
