setRow = ({ id, image, created_at, updated_at })=> (`
    <tr class='row${id}'>
        <td class='btnActionContainer'>
            <button type="button" data-id="${id}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                <i class="material-icons">delete</i>
            </button>
        </td>
        <td class="image-${id}">
            <a href="${image}" target="_blank">
                <img src="${image}" alt="image" width="117" height="32">
            </a>
        </td>
        <td data-order="${created_at}">${created_at}</td>
        <td class="updated_at-${id}" data-order="${updated_at}">${updated_at}</td>
    </tr>
`);

setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

$(document).ready(() => {

    /* Variable declaration - initialization */
    const table = $(document).find('table');
    const addForm = $(document).find('.addForm');
    const addModal = $(document).find('.addModal');
    const editForm = $(document).find('.editForm');
    const editModal = $(document).find('.editModal');
    const deleteForm = $(document).find('.deleteForm');
    const deleteModal = $(document).find('.deleteModal');

    const dt = table.DataTable({
        order: [[3, "desc"]],
        "columnDefs": [
            { "orderable": false, "targets": [0, 1] },
            { "searchable": false, "targets": [0, 1] }
        ],
        "columns": [
            { "width": "10%" },
            { "width": "40%" },
            { "width": "25%" },
            { "width": "25%" },
        ]
    });

    addForm.validate({
        rules: {
            image: {
                required: true,
                accept: "image/jpg,image/jpeg,image/png"
            },
        },
        messages: {
            image: {
                accept: "Please select a valid image (jpg, jpeg, png)"
            },
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    addForm.submit(event => {
        event.preventDefault();

        if (addForm.valid()) {
            const formData = new FormData(addForm[0]);

            $.ajax({
                url: addForm.attr('action'),
                type: addForm.attr('method'),
                beforeSend: () => {
                    $('.btnSave').html('<i class="fa fa-spinner fa-spin"></i> Saving...');
                },
                data: formData,
                processData: false,
                contentType: false,
                success: response => {
                    if (response.success) {
                        const row = setRow(response.data);

                        dt.row.add($(row)).draw(false);
                        addForm.trigger("reset");

                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnSave').html('SAVE')
            });
        }
    });

    deleteModal.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const image = $(`.image-${id}`).find('img').attr('src');

        $('.delete-id').val(id);
        $('.delete-image').attr('src', image);
    });

    deleteForm.submit(event => {
        event.preventDefault();
        const _method = deleteForm.attr('method');
        const id = $('.delete-id').val();

        $.ajax({
            url: `/slides/${id}`,
            type: _method,
            beforeSend: () => {
                $('.btnDelete').html('<i class="fa fa-spinner fa-spin"></i> Deleting...');
            },
            data: { _method },
            success: response => {
                if (response.success) {
                    deleteModal.modal('hide');
                    iziToast.success({
                        title: 'Success',
                        message: response.success
                    });
                    dt.row(`.row${id}`).remove().draw(false);
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: setErrors(response.error),
                        timeout: false
                    });
                }
            },
            error: response => console.log(response),
            complete: () => $('.btnDelete').html('DELETE')
        });
    });

});
