@extends('layouts.app')

@section('title', 'Contact Us')

@section('content')
    <section class="bgimage bg-imageURL-contact">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Contact Us</h2>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid custom-separator">
        <div class="container">
            <div id="id-contact" class="row">
                <div class="col-md-3">
                    <div><i class="fa fa-map-marker"></i></div>
                    <h3>Address</h3>
                    <div id="contact-address">{{ $contact->address }}</div>
                </div>

                <div class="col-md-3">
                    <div><i class="fa fa-phone"></i></div>
                    <h3>Phone Number</h3>
                    <div id="contact-phone">{{ $contact->mobile_no }}</div>
                    @if ($contact->telephone_no)
                        <br/>{{ $contact->telephone_no }}
                    @endif
                </div>

                <div class="col-md-3">
                    <div><i class="fa fa-envelope"></i></div>
                    <h3>Email</h3>
                    <div id="contact-email"><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></div>
                </div>

                <div class="col-md-3">
                    <div><i class="far fa-clock"></i></div>
                    <h3>Business Hours</h3>
                    <div id="contact-business-hours">{{ $contact->business_hours }}</div>
                </div>
            </div>
        </div>
    </div>

    <section class="default-margin">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2283.267943184919!2d121.07927343910829!3d14.768245129664997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397a55610b52597%3A0xb3594fee9790e4b1!2sJohnlyntin+Hardware!5e0!3m2!1sen!2sph!4v1522652782060" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen>
                        <img src="{{ asset('images/frontend/map-placeholder.jpg') }}">
                    </iframe>
                </div>
            </div>
        </div>
    </section>
@endsection
