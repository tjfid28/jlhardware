<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SuppliersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $suppliers = User::where('role', 'supplier')->latest()->get();
        return view('suppliers.index', compact('suppliers'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:191|unique:users',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'mobile_no' => 'required|digits:10',
            'telephone_no' => 'nullable|digits:7',
            'building_street_info' => 'required|string|max:191',
            'barangay' => 'required|string|max:191',
            'city' => 'required|string|max:191',
            'province' => 'required|string|max:191',
            'region' => 'required|string|max:191',
            'postal_code' => 'required|digits:4',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 'supplier'
        ]);

        Profile::create([
            'user_id' => $user->id,
            'mobile_no' => $request->mobile_no,
            'telephone_no' => $request->telephone_no,
            'building_street_info' => $request->building_street_info,
            'barangay' => $request->barangay,
            'city' => $request->city,
            'province' => $request->province,
            'region' => $request->region,
            'postal_code' => $request->postal_code
        ]);

        $user->load('profile');
        $user->formattedMobileNo = $user->profile->getFormattedMobileNo();
        $user->formattedTelephoneNo = $user->profile->getFormattedTelephoneNo();
        $user->address = $user->profile->getAddress();

        return response()->json(['success' => 'Supplier successfully saved', 'data' => $user]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:191|unique:users',
            'email' => "required|string|email|max:191|unique:users,email,$request->id,id",
            'password' => 'nullable|string|min:6|confirmed',
            'mobile_no' => 'required|digits:10',
            'telephone_no' => 'nullable|digits:7',
            'building_street_info' => 'required|string|max:191',
            'barangay' => 'required|string|max:191',
            'city' => 'required|string|max:191',
            'province' => 'required|string|max:191',
            'region' => 'required|string|max:191',
            'postal_code' => 'required|digits:4',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $user = User::findOrFail($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        if (!is_null($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        $profile = $user->profile;
        $profile->mobile_no = $request->mobile_no;
        $profile->telephone_no = $request->telephone_no;
        $profile->building_street_info = $request->building_street_info;
        $profile->barangay = $request->barangay;
        $profile->city = $request->city;
        $profile->province = $request->province;
        $profile->region = $request->region;
        $profile->postal_code = $request->postal_code;
        $user->profile()->save($profile);

        $user->load('profile');
        $user->formattedMobileNo = $user->profile->getFormattedMobileNo();
        $user->formattedTelephoneNo = $user->profile->getFormattedTelephoneNo();
        $user->address = $user->profile->getAddress();

        return response()->json(['success' => 'Supplier successfully updated', 'data' => $user]);
    }

    public function destroy($id)
    {
        try {
            User::destroy($id);
            return response()->json(['success' => 'Supplier successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }
}
