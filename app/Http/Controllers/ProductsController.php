<?php

namespace App\Http\Controllers;

use App\User;
use App\Brand;
use App\Product;
use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff')->except(['show', 'search']);
    }

    public function index()
    {
        $brands = Brand::all()->sortBy('name');
        $categories = Category::all()->sortBy('name');
        $products = Product::latest('updated_at')->get();
        $suppliers = User::where('role', 'supplier')->get()->sortBy('name');
        return view('products.index', compact('products', 'brands', 'categories', 'suppliers'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|mimes:jpeg,jpg,png',
            'name' => 'required|unique:products,name,NULL,id',
            'brand_id' => 'required',
            'subcategory_id' => 'required',
            'supplier_id' => 'required',
            'price' => 'required',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $extension = '.' . $request->image->getClientOriginalExtension();
        $imageName = sha1(time()) . $extension;
        $subcategory = Subcategory::findOrFail($request->subcategory_id);

        $product = Product::create([
            'image' => $imageName,
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'brand_id' => $request->brand_id,
            'category_id' => $subcategory->category_id,
            'subcategory_id' => $request->subcategory_id,
            'supplier_id' => $request->supplier_id,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description
        ]);

        $request->image->move(public_path('images/products'), $imageName);
        $product->image = $product->getImageUrl();
        $product->load('brand', 'category', 'subcategory', 'supplier');

        return response()->json(['success' => 'Product successfully saved', 'data' => $product]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png',
            'name' => "required|unique:products,name,$request->id,id",
            'brand_id' => 'required',
            'subcategory_id' => 'required',
            'supplier_id' => 'required',
            'price' => 'required',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $subcategory = Subcategory::findOrFail($request->subcategory_id);

        $product = Product::findOrFail($request->id);
        $product->name = $request->name;
        $product->slug = str_slug($request->name);
        $product->brand_id = $request->brand_id;
        $product->category_id = $subcategory->category_id;
        $product->subcategory_id = $request->subcategory_id;
        $product->supplier_id = $request->supplier_id;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->description = $request->description;

        if ($request->hasFile('image')) {
            $extension = '.' . $request->image->getClientOriginalExtension();
            $imageName = sha1(time()) . $extension;
            unlink($product->getImagePath());
            $product->image = $imageName;
            $request->image->move(public_path('images/products'), $imageName);
        }

        $product->save();
        $product->load('brand', 'category', 'subcategory', 'supplier');
        $product->image = $product->getImageUrl();

        return response()->json(['success' => 'Product successfully updated', 'data' => $product]);
    }

    public function destroy($id)
    {
        try {
            $product = Product::find($id);
            unlink($product->getImagePath());
            $product->delete();
            return response()->json(['success' => 'Product successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }

    public function show($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        return view('products.show', compact('product'));
    }

    public function search(Request $request)
    {
        $products = Product::with(['brand', 'category'])->search($request->filter);

        if ($products->count() > 0) {
            if ($request->filled('brand')) {
                $brand = $request->brand;
                $products = $products->whereHas('brand', function ($q) use ($brand) {
                    $q->where('slug', '=', $brand);
                });
            }

            if ($request->filled('category')) {
                $category = $request->category;
                $products = $products->whereHas('category', function ($q) use ($category) {
                    $q->where('slug', '=', $category);
                });
            }
        }

        $products = $products->paginate(12);

        return view('products.search', compact('products'));
    }
}
