@extends('layouts.app')

@section('title', 'Search - ' . Request::get('filter'))

@section('content')
    <section class="bg-texture">
        <div class="container">
            <div class="home-header" style="margin-bottom: 30px;"><strong>Search results for: </strong>{{ Request::get('filter') }}</div>

            <div class="container box-products">
                <div class="row product-container">
                    @forelse ($products as $key => $product)
                        <div class="col-md-3" style="margin-bottom: 30px;">
                            <div class='product-image-wrapper'>
                                <div class="main-prod">
                                    <div class="productinfo text-center">
                                        <p>
                                            <a href="{{ route('products.show', $product->slug) }}" rel="bookmark" title="{{ $product->name }}">
                                                <img src="{{ $product->getImageUrl() }}" alt="{{ $product->name }}" title="{{ $product->name }}" width="150" height="150" />
                                            </a>
                                        </p>

                                        <p>
                                            <a href="{{ route('products.show', $product->slug) }}" rel="bookmark" title="{{ $product->name }}">
                                                {{ $product->name }}
                                            </a>
                                        </p>

                                        <p>Price: {{ number_format($product->price, 2) }}</p>

                                        <a href="{{ route('products.show', $product->slug) }}" class="btn btn-default add-to-cart">
                                            <i class="fa fa-shopping-cart"></i> View Details
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <h2 class="text-center">No records found</h2>
                    @endforelse

                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
