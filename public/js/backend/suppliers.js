setRow = ({ id, name, email, password, created_at, updated_at, formattedMobileNo, formattedTelephoneNo, address, profile: { mobile_no, telephone_no, building_street_info, barangay, city, province, region, postal_code } })=> (`
    <tr class="row${id}">
        <td class="btnActionContainer">
            <button type="button" data-id="${id}" class="btn btn-success btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".editModal" title="edit">
                <i class="material-icons">edit</i>
            </button>
            &nbsp;
            <button type="button" data-id="${id}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                <i class="material-icons">delete</i>
            </button>
        </td>
        <td class="name-${id}">${name}</td>
        <td class="email-${id}">${email}</td>
        <td class="mobile_no-${id}" data-mobile_no="${mobile_no}">
            ${formattedMobileNo}
        </td>
        <td class="telephone_no-${id}" data-telephone_no="${telephone_no}">
            ${telephone_no ? formattedTelephoneNo : ''}
        </td>
        <td class="address-${id}" data-building_street_info="${building_street_info}" data-barangay="${barangay}" data-city="${city}" data-province="${province}" data-region="${region}" data-postal_code="${postal_code}">
            ${address}
        </td>
        <td data-order="${created_at}">${created_at}</td>
        <td class="updated_at-${id}" data-order="${updated_at}">
            ${updated_at}
        </td>
    </tr>
`);

setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

$(document).ready(() => {

    /* Variable declaration - initialization */
    const table = $(document).find('table');
    const addForm = $(document).find('.addForm');
    const addModal = $(document).find('.addModal');
    const editForm = $(document).find('.editForm');
    const editModal = $(document).find('.editModal');
    const deleteForm = $(document).find('.deleteForm');
    const deleteModal = $(document).find('.deleteModal');

    const dt = table.DataTable({
        order: [[7, "desc"]],
        "columnDefs": [
            { "orderable": false, "targets": [0, 3, 4, 5] },
            { "searchable": false, "targets": 0 }
        ]
    });

    addForm.validate({
        rules: {
            name: {
                required: true,
                normalizer: value => $.trim(value)
            },
            email: {
                required: true,
                email: true,
                normalizer: value => $.trim(value)
            },
            password: {
                required: true,
                minlength: 6,
                normalizer: value => $.trim(value)
            },
            password_confirmation: {
                equalTo: '.add-password'
            },
            mobile_no: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
                normalizer: value => $.trim(value),
            },
            telephone_no: {
                digits: true,
                minlength: 7,
                maxlength: 7,
                normalizer: value => $.trim(value),
            },
            building_street_info: {
                required: true,
                normalizer: value => $.trim(value)
            },
            barangay: {
                required: true,
                normalizer: value => $.trim(value)
            },
            city: {
                required: true,
                normalizer: value => $.trim(value)
            },
            province: {
                required: true,
                normalizer: value => $.trim(value)
            },
            region: {
                required: true,
                normalizer: value => $.trim(value)
            },
            postal_code: {
                required: true,
                digits: true,
                minlength: 4,
                maxlength: 4,
                normalizer: value => $.trim(value)
            }
        },
        messages: {
            password_confirmation: {
                equalTo: "Password does not match"
            },
            mobile_no : {
                minlength: "Please enter a valid mobile no.",
                maxlength: "Please enter a valid mobile no.",
            },
            telephone_no : {
                minlength: "Please enter a valid telephone no.",
                maxlength: "Please enter a valid telephone no.",
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    addForm.submit(event => {
        event.preventDefault();

        if (addForm.valid()) {
            const formData = new FormData(addForm[0]);

            $.ajax({
                url: addForm.attr('action'),
                type: addForm.attr('method'),
                beforeSend: () => {
                    $('.btnSave').html('<i class="fa fa-spinner fa-spin"></i> Saving...');
                },
                processData: false,
                contentType: false,
                data: formData,
                success: response => {
                    if (response.success) {
                        const row = setRow(response.data);

                        dt.row.add($(row)).draw(false);
                        addForm.trigger("reset");

                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnSave').html('SAVE')
            });
        }

    });

    editForm.validate({
        rules: {
            name: {
                required: true,
                normalizer: value => $.trim(value)
            },
            email: {
                required: true,
                email: true,
                normalizer: value => $.trim(value)
            },
            password: {
                minlength: 6,
                normalizer: value => $.trim(value)
            },
            password_confirmation: {
                equalTo: '.edit-password'
            },
            mobile_no: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
                normalizer: value => $.trim(value),
            },
            telephone_no: {
                digits: true,
                minlength: 7,
                maxlength: 7,
                normalizer: value => $.trim(value),
            },
            building_street_info: {
                required: true,
                normalizer: value => $.trim(value)
            },
            barangay: {
                required: true,
                normalizer: value => $.trim(value)
            },
            city: {
                required: true,
                normalizer: value => $.trim(value)
            },
            province: {
                required: true,
                normalizer: value => $.trim(value)
            },
            region: {
                required: true,
                normalizer: value => $.trim(value)
            },
            postal_code: {
                required: true,
                digits: true,
                minlength: 4,
                maxlength: 4,
                normalizer: value => $.trim(value)
            }
        },
        messages: {
            password_confirmation: {
                equalTo: "Password does not match"
            },
            mobile_no : {
                minlength: "Please enter a valid mobile no.",
                maxlength: "Please enter a valid mobile no.",
            },
            telephone_no : {
                minlength: "Please enter a valid telephone no.",
                maxlength: "Please enter a valid telephone no.",
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    editModal.on('shown.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const name = $(`.name-${id}`).text();
        const email = $(`.email-${id}`).text();
        const mobile_no = $(`.mobile_no-${id}`).data('mobile_no');
        const telephone_no = $(`.telephone_no-${id}`).data('telephone_no');
        const building_street_info = $(`.address-${id}`).data('building_street_info');
        const barangay = $(`.address-${id}`).data('barangay');
        const city = $(`.address-${id}`).data('city');
        const province = $(`.address-${id}`).data('province');
        const region = $(`.address-${id}`).data('region');
        const postal_code = $(`.address-${id}`).data('postal_code');

        // console.log(name, email, mobile_no, telephone_no);

        $('.edit-id').val(id);
        $('.edit-postal_code').val(postal_code).focus();
        $('.edit-region').val(region).focus();
        $('.edit-province').val(province).focus();
        $('.edit-city').val(city).focus();
        $('.edit-barangay').val(barangay).focus();
        $('.edit-building_street_info').val(building_street_info).focus();
        $('.edit-telephone_no').val(telephone_no).focus();
        $('.edit-mobile_no').val(mobile_no).focus();
        $('.edit-email').val(email).focus();
        $('.edit-name').val(name).focus();
    });

    editForm.submit(event => {
        event.preventDefault();

        if (editForm.valid()) {
            const _method = editForm.attr('method');
            const id = $('.edit-id').val();
            const formData = new FormData(editForm[0]);
            formData.append('_method', _method);

            $.ajax({
                url: `/suppliers/${id}`,
                type: 'POST',
                beforeSend: () => {
                    $('.btnUpdate').html('<i class="fa fa-spinner fa-spin"></i> Updating...');
                },
                processData: false,
                contentType: false,
                data: formData,
                success: response => {
                    if (response.success) {
                        const { name, email, updated_at, formattedMobileNo, formattedTelephoneNo, address, profile: { mobile_no, telephone_no, building_street_info, barangay, city, province, region, postal_code } } = response.data;
                        const tempRow = dt.row(`.row${id}`).data();
                        tempRow[1] = name;
                        tempRow[2] = email;
                        tempRow[3] = formattedMobileNo;
                        tempRow[4] = telephone_no ? formattedMobileNo : '';
                        tempRow[5] = address;
                        tempRow[7].display = updated_at;

                        $(`.mobile_no-${id}`).data('mobile_no', mobile_no);
                        $(`.telephone_no-${id}`).data('telephone_no', telephone_no);
                        $(`.address-${id}`).data('building_street_info', building_street_info);
                        $(`.address-${id}`).data('barangay', barangay);
                        $(`.address-${id}`).data('city', city);
                        $(`.address-${id}`).data('province', province);
                        $(`.address-${id}`).data('region', region);
                        $(`.address-${id}`).data('postal_code', postal_code);
                        $(`.updated_at-${id}`).attr('data-order', updated_at);
                        dt.row(`.row${id}`).data(tempRow).invalidate().draw(false);

                        editModal.modal('hide');
                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnUpdate').html('UPDATE')
            });
        }
    });

    deleteModal.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const name = $(`.name-${id}`).text();
        const email = $(`.email-${id}`).text();
        const mobile_no = $(`.mobile_no-${id}`).text();
        const telephone_no = $(`.telephone_no-${id}`).text();
        const address = $(`.address-${id}`).text();

        $('.delete-id').val(id);
        $('.delete-name').val(name);
        $('.delete-email').val(email);
        $('.delete-mobile_no').val($.trim(mobile_no));
        $('.delete-telephone_no').val($.trim(telephone_no));
        $('.delete-address').val($.trim(address));
    });

    deleteForm.submit(event => {
        event.preventDefault();
        const _method = deleteForm.attr('method');
        const id = $('.delete-id').val();

        $.ajax({
            url: `/suppliers/${id}`,
            type: _method,
            beforeSend: () => {
                $('.btnDelete').html('<i class="fa fa-spinner fa-spin"></i> Deleting...');
            },
            data: { _method },
            success: response => {
                if (response.success) {
                    dt.row(`.row${id}`).remove().draw(false);
                    deleteModal.modal('hide');
                    iziToast.success({
                        title: 'Success',
                        message: response.success
                    });
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: response.error,
                        timeout: false
                    });
                }
            },
            error: response => console.log(response),
            complete: () => $('.btnDelete').html('DELETE')
        });
    });

});
