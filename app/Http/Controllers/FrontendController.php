<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Term;
use App\Slide;
use App\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $slides = Slide::latest('updated_at')->get();
        $products = Product::latest()->get();
        return view('frontend.index', compact('slides', 'products'));
    }

    public function terms()
    {
        $terms = Term::latest('updated_at')->get();
        return view('frontend.terms', compact('terms'));
    }

    public function faqs()
    {
        $faqs = Faq::latest('updated_at')->get();
        return view('frontend.faqs', compact('faqs'));
    }
}
