@extends('layouts.app')

@section('title', 'Profile')

@section('styles')
    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">
@endsection

@php
    $profile = Auth::user()->profile;
@endphp

@section('content')
    <section class="bg-texture">
        <div class="container" style="padding-top: 20px">
            <div class="row">
                <div class="panel panel-success">
                    <div class="panel-heading text-center"><strong style="font-size: 16px;">Update Your Profile</strong></div>
                    <div class="panel-body">
                        <div id="id-about" class="col-md-6 col-md-offset-3">
                            @include('includes.message')

                            <form action="{{ route('profile.update') }}" method="POST" class="form-profile">
                                @csrf
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" placeholder="Email" value="{{ auth()->user()->email }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="mobile_no">Mobile No.*</label>
                                    <input type="number" name="mobile_no" class="form-control" placeholder="Ex. 9123456789" value="{{ $profile->mobile_no }}">
                                </div>

                                <div class="form-group">
                                    <label for="telephone_no">Telephone No. <i>(Optional)</i></label>
                                    <input type="number" name="telephone_no" class="form-control" placeholder="Ex. 1234567" value="{{ $profile->telephone_no }}">
                                </div>

                                <div class="form-group">
                                    <label for="building_street_info">Bldg. / Street / Subd.*</label>
                                    <textarea name="building_street_info" class="form-control" rows="3" resizable="false">{{ $profile->building_street_info }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="barangay">Barangay*</label>
                                    <input type="text" name="barangay" class="form-control" placeholder="Barangay" value="{{ $profile->barangay }}">
                                </div>

                                <div class="form-group">
                                    <label for="city">City*</label>
                                    <input type="text" name="city" class="form-control" placeholder="City" value="{{ $profile->city }}">
                                </div>

                                <div class="form-group">
                                    <label for="province">Province*</label>
                                    <input type="text" name="province" class="form-control" placeholder="Province" value="{{ $profile->province }}">
                                </div>

                                <div class="form-group">
                                    <label for="region">Region*</label>
                                    <input type="text" name="region" class="form-control" placeholder="Region" value="{{ $profile->region }}">
                                </div>

                                <div class="form-group">
                                    <label for="postal_code">Postal Code*</label>
                                    <input type="text" name="postal_code" class="form-control" placeholder="Postal Code" value="{{ $profile->postal_code }}">
                                </div>

                                <button type="submit" class="btn btn-success btn-default center-block">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @section('scripts')
        <!-- Validation Plugin Js -->
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

        <!-- iziToast Js -->
        <script src="{{ asset('js/iziToast.min.js') }}"></script>

        <!-- Additional JS -->
        <script type="text/javascript" src="{{ asset('js/frontend/profile.js') }}"></script>
    @endsection
@endsection
