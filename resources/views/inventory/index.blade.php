@extends('layouts.backend')

@section('title', $title)

@section('styles')
    <!-- JQuery dataTables CSS -->
    <link href="{{ asset('css/backend/datatables.min.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        {{ strtoupper($title) }}
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-12 col-xs-12">
                            <h2>List of Products</h2>
                        </div>
                    </div>
                </div>

                <div class="body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">Image</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Brand Id</th>
                                <th class="text-center">Brand</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Subcategory Id</th>
                                <th class="text-center">Subcategory</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Supplier</th>
                                <th class="text-center">Order Count</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">Updated At</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @forelse ($products as $product)
                                <tr class="row{{ $product->id }}">
                                    <td class="image-{{ $product->id }}">
                                        <a href="{{ $product->getImageUrl() }}" target="_blank">
                                            <img src="{{ $product->getImageUrl() }}" alt="{{ $product->getImageUrl() }}" width="48" height="24">
                                        </a>
                                    </td>
                                    <td class="name-{{ $product->id }}">{{ $product->name }}</td>
                                    <td class="brand_id-{{ $product->id }}">{{ $product->brand_id }}</td>
                                    <td class="brand_name-{{ $product->id }}">{{ $product->brand->name }}</td>
                                    <td class="category-{{ $product->id }}">{{ $product->category->name }}</td>
                                    <td class="subcategory_id-{{ $product->id }}">{{ $product->subcategory_id }}</td>
                                    <td class="subcategory_name-{{ $product->id }}">{{ $product->subcategory->name }}</td>
                                    <td class="price-{{ $product->id }}" data-price="{{ $product->price }}">{{ number_format($product->price, 2) }}</td>
                                    <td class="quantity-{{ $product->id }}">{{ $product->quantity }}</td>
                                    <td class="supplier-{{ $product->id }}">{{ $product->supplier->name }}</td>
                                    <td class="order_count-{{ $product->id }}">{{ $product->order_count }}</td>
                                    <td class="description-{{ $product->id }}">{{ $product->description }}</td>
                                    <td data-order="{{ $product->created_at }}">{{ $product->created_at }}</td>
                                    <td class="updated_at-{{ $product->id }}" data-order="{{ $product->updated_at }}">{{ $product->updated_at }}</td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- JQuery dataTables JS -->
    <script type="text/javascript" src="{{ asset('js/backend/datatables.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/inventory.js') }}"></script>
@endsection
