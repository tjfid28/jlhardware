<?php

namespace App\Http\Controllers;

use App\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TermsController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function index()
    {
        $terms = Term::latest()->get();
        return view('terms.index', compact('terms'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $term = Term::create([
            'description' => $request->description
        ]);

        return response()->json(['success' => 'Term and condition successfully saved', 'data' => $term]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $term = Term::find($request->id);
        $term->description = $request->description;
        $term->save();

        return response()->json(['success' => 'Term and condition successfully updated', 'data' => $term]);
    }

    public function destroy($id)
    {
        try {
            Term::destroy($id);
            return response()->json(['success' => 'Term and condition successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }
}
