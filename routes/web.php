<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('home');
Route::get('/terms-and-conditions', 'FrontendController@terms')->name('terms');
Route::get('/frequently-asked-questions', 'FrontendController@faqs')->name('faqs');
// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::get('/dashboard', 'BackendController@index')->name('dashboard');
Route::resource('brands', 'BrandsController');
Route::resource('categories', 'CategoriesController');
Route::resource('subcategories', 'SubcategoriesController');
Route::get('/products/search', 'ProductsController@search')->name('products.search');
Route::patch('products/{product}/change-type', 'ProductsController@changeType')->name('products.changeType');
Route::resource('products', 'ProductsController');
Route::delete('cart/empty', 'CartController@empty')->name('cart.empty');
Route::resource('cart', 'CartController');
Route::resource('orders', 'OrdersController');
Route::post('orders/add-item', 'OrdersController@addItem')->name('orders.addItem');
Route::patch('orders/update-item/{item}', 'OrdersController@updateItem')->name('orders.updateItem');
Route::delete('orders/delete-item/{item}', 'OrdersController@deleteItem')->name('orders.deleteItem');
Route::delete('orders/items/empty', 'OrdersController@empty')->name('orders.empty');
Route::post('orders/checkout', 'OrdersController@checkout')->name('orders.checkout');
Route::patch('purchase-orders/cancel/{purchaseOrder}', 'PurchaseOrdersController@cancel')->name('purchase-orders.cancel')->middleware('admin');
Route::patch('purchase-orders/receive/{purchaseOrder}', 'PurchaseOrdersController@receive')->name('purchase-orders.receive')->middleware('admin');
Route::patch('purchase-orders/complete/{purchaseOrder}', 'PurchaseOrdersController@complete')->name('purchase-orders.complete')->middleware('supplier');
Route::resource('purchase-orders', 'PurchaseOrdersController')->middleware('admin-supplier');
Route::resource('users', 'UsersController');
Route::get('/verify/{token}', 'UsersController@verify')->name('verify');
Route::resource('suppliers', 'SuppliersController');
Route::resource('terms', 'TermsController');
Route::resource('faqs', 'FaqsController');
Route::resource('slides', 'SlidesController');

/* About */
Route::get('about-us', 'AboutController@index')->name('about');
Route::get('about/edit', 'AboutController@edit')->name('about.edit');
Route::patch('about', 'AboutController@update')->name('about.update');

/* Contact */
Route::get('contact-us', 'ContactsController@index')->name('contacts');
Route::get('contact/edit', 'ContactsController@edit')->name('contacts.edit');
Route::patch('contact', 'ContactsController@update')->name('contacts.update');

/* Profile */
Route::get('user/profile', 'ProfilesController@index')->name('profile');
Route::patch('user/profile', 'ProfilesController@update')->name('profile.update');

/* Customer's Orders */
Route::get('user/orders', 'UsersController@orders')->name('user.orders');
Route::post('user/cancel-order', 'UsersController@cancelOrder')->name('user.cancelOrder');
Route::delete('user/cancel-order-item', 'UsersController@cancelOrderItem')->name('user.cancelOrderItem');

/* Inventory */
Route::get('inventory/fast-moving-items', 'InventoryController@fastMovingItems')->name('inventory.fastMovingItems');
Route::get('inventory/slow-moving-items', 'InventoryController@slowMovingItems')->name('inventory.slowMovingItems');
Route::get('inventory/critical-level', 'InventoryController@criticalLevel')->name('inventory.criticalLevel');

/* Report */
Route::get('reports', 'ReportsController@index')->name('reports.index');
Route::get('reports/inventory', 'ReportsController@inventory')->name('reports.inventory');
Route::get('reports/orders', 'ReportsController@orders')->name('reports.orders');
Route::get('reports/sales', 'ReportsController@sales')->name('reports.sales');
Route::get('reports/purchase-orders', 'ReportsController@purchaseOrders')->name('reports.purchaseOrders');
