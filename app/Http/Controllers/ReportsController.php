<?php

namespace App\Http\Controllers;

use PdfReport;
use App\Product;
use ExcelReport;
use App\OrderItem;
use Carbon\Carbon;
use App\PurchaseOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff');
    }

    public function index()
    {
        return view('reports.index');
    }

    public function inventory(Request $request)
    {
        $fileName = 'inventory-report-' . time();
        $title = 'Inventory Report';
        $type = $request->type;
        $startDate = $request->startDate;
        $endDate = $request->endDate;

        $meta = [
            'Created from' => $startDate . ' To ' . $endDate,
            'Inventory Type' => ucfirst($type)
        ];

        $queryBuilder = null;

        switch ($request->type) {
            case 'all':
                $queryBuilder = Product::whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;

            case 'critical':
                $queryBuilder = Product::where('quantity', '<=', config('env.critical_level'))
                    ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;

            case 'fast-moving':
                $queryBuilder = Product::where('order_count', '>=', config('env.fast_moving_count'))
                    ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;

            case 'slow-moving':
                $queryBuilder = Product::whereBetween('order_count', [1, config('env.fast_moving_count') - 1])
                    ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;
        }

        $columns = [
            'Name',
            'Brand' => function ($product) {
                return $product->brand->name;
            },
            'Category' => function ($product) {
                return $product->category->name;
            },
            'Subcategory' => function ($product) {
                return $product->subcategory->name;
            },
            'Supplier' => function ($product) {
                return $product->supplier->name;
            },
            'Price' => function ($product) {
                return number_format($product->price, 2);
            },
            'Quantity' => function ($product) {
                return number_format($product->quantity);
            }
        ];

        switch ($request->fileType) {
            case 'pdf':
                return PdfReport::of($title, $meta, $queryBuilder, $columns)
                    ->setCss([
                        '.head-content' => 'border-width: 1px',
                    ])
                    ->editColumns(['Price', 'Quantity'], [
                        'class' => 'right'
                    ])
                    ->download($fileName);

            case 'excel':
                return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                    ->setCss([
                        '.head-content' => 'border-width: 1px',
                    ])
                    ->editColumns(['Price', 'Quantity'], [
                        'class' => 'right'
                    ])
                    ->download($fileName);
        }
    }

    public function orders(Request $request)
    {
        $fileName = 'orders-report-' . time();
        $title = 'Orders Report';
        $status = $request->status;
        $startDate = $request->startDate;
        $endDate = $request->endDate;

        $meta = [
            'Ordered from' => $startDate . ' To ' . $endDate,
            'Order status' => ucfirst($status)
        ];

        $queryBuilder = null;

        switch ($request->status) {
            case 'all':
                $queryBuilder = OrderItem::whereHas('order', function ($q) use ($startDate, $endDate) {
                    $q->where('status_id', '<>', 4)
                        ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                        ->latest('created_at');
                });
                break;

            case 'pending':
                $queryBuilder = OrderItem::whereHas('order', function ($q) use ($startDate, $endDate) {
                    $q->where('status_id', 1)
                        ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                        ->latest('created_at');
                });
                break;

            case 'to-ship':
                $queryBuilder = OrderItem::whereHas('order', function ($q) use ($startDate, $endDate) {
                    $q->where('status_id', 2)
                        ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                        ->latest('created_at');
                });
                break;

            case 'to-receive':
                $queryBuilder = OrderItem::whereHas('order', function ($q) use ($startDate, $endDate) {
                    $q->where('status_id', 3)
                        ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                        ->latest('created_at');
                });
                break;

            case 'cancelled':
                $queryBuilder = OrderItem::whereHas('order', function ($q) use ($startDate, $endDate) {
                    $q->where('status_id', 5)
                        ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                        ->latest('created_at');
                });
                break;
        }

        $columns = [
            'Order#' => function ($item) {
                return $item->order->getOrderNumber();
            },
            'Customer' => function ($item) {
                return $item->order->user->name;
            },
            'Status' => function ($item) {
                return $item->order->status->name;
            },
            'Date Ordered' => function ($item) {
                return $item->order->created_at;
            },
            'Product' => function ($item) {
                return $item->product->name;
            },
            'Quantity' => function ($item) {
                return number_format($item->quantity);
            },
            'Price' => function ($item) {
                return number_format($item->product->price, 2);
            },
            'Sub Total' => function ($item) {
                return $item->product->price * $item->quantity;
            }
        ];

        switch ($request->fileType) {
            case 'pdf':
                return PdfReport::of($title, $meta, $queryBuilder, $columns)
                    ->editColumn('Date Ordered', [
                        'displayAs' => function ($item) {
                            return $item->order->created_at->format('M d, Y');
                        }
                    ])
                    ->editColumn('Price', [
                        'class' => 'right'
                    ])
                    ->editColumn('Sub Total', [
                        'class' => 'right bold',
                        'displayAs' => function ($item) {
                            return number_format($item->product->price * $item->quantity, 2);
                        }
                    ])
                    ->groupBy('Order#')
                    ->showTotal([
                        'Sub Total' => 'point'
                    ])
                    ->download($fileName);

            case 'excel':
                return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                    ->editColumn('Date Ordered', [
                        'displayAs' => function ($item) {
                            return $item->order->created_at->format('M d, Y');
                        }
                    ])
                    ->editColumn('Price', [
                        'class' => 'right'
                    ])
                    ->editColumn('Sub Total', [
                        'class' => 'right bold',
                        'displayAs' => function ($item) {
                            return number_format($item->product->price * $item->quantity, 2);
                        }
                    ])
                    ->groupBy('Order#')
                    ->showTotal([
                        'Sub Total' => 'point'
                    ])
                    ->download($fileName);
        }
    }

    public function sales(Request $request)
    {
        $fileName = 'sales-report-' . time();
        $title = 'Sales Report';
        $startDate = $request->startDate;
        $endDate = $request->endDate;

        $meta = [
            'Date from' => $startDate . ' To ' . $endDate
        ];

        $queryBuilder = null;

        $queryBuilder = OrderItem::whereHas('order', function ($q) use ($startDate, $endDate) {
            $q->where('status_id', 4)
                ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                ->latest('created_at');
        });

        $columns = [
            'Order#' => function ($item) {
                return $item->order->getOrderNumber();
            },
            'Customer' => function ($item) {
                return $item->order->user->isAdmin() || $item->order->user->isStaff() ? $item->order->name : $item->order->user->name;
            },
            'Date Ordered' => function ($item) {
                return $item->order->created_at;
            },
            'Product' => function ($item) {
                return $item->product->name;
            },
            'Quantity' => function ($item) {
                return number_format($item->quantity);
            },
            'Price' => function ($item) {
                return number_format($item->product->price, 2);
            },
            'Sub Total' => function ($item) {
                return $item->product->price * $item->quantity;
            }
        ];

        switch ($request->fileType) {
            case 'pdf':
                return PdfReport::of($title, $meta, $queryBuilder, $columns)
                    ->editColumn('Date Ordered', [
                        'displayAs' => function ($item) {
                            return $item->order->created_at->format('M d, Y');
                        }
                    ])
                    ->editColumn('Price', [
                        'class' => 'right'
                    ])
                    ->editColumn('Sub Total', [
                        'class' => 'right bold',
                        'displayAs' => function ($item) {
                            return number_format($item->product->price * $item->quantity, 2);
                        }
                    ])
                    ->groupBy('Order#')
                    ->showTotal([
                        'Sub Total' => 'point'
                    ])
                    ->download($fileName);

            case 'excel':
                return ExcelReport::of($title, $meta, $queryBuilder, $columns)
                    ->editColumn('Date Ordered', [
                        'displayAs' => function ($item) {
                            return $item->order->created_at->format('M d, Y');
                        }
                    ])
                    ->editColumn('Price', [
                        'class' => 'right'
                    ])
                    ->editColumn('Sub Total', [
                        'class' => 'right bold',
                        'displayAs' => function ($item) {
                            return number_format($item->product->price * $item->quantity, 2);
                        }
                    ])
                    ->groupBy('Order#')
                    ->showTotal([
                        'Sub Total' => 'point'
                    ])
                    ->download($fileName);
        }
    }

    public function purchaseOrders(Request $request)
    {
        $fileName = 'purchase-orders-report-' . time();
        $title = 'Purchase Orders Report';
        $status = $request->status;
        $startDate = $request->startDate;
        $endDate = $request->endDate;

        $meta = [
            'Ordered from' => $startDate . ' To ' . $endDate,
            'Purchase Order status' => ucfirst($status)
        ];

        $queryBuilder = null;

        switch ($request->status) {
            case 'all':
                $queryBuilder = PurchaseOrder::whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;

            case 'pending':
                $queryBuilder = PurchaseOrder::where('status_id', 1)
                    ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;

            case 'completed':
                $queryBuilder = PurchaseOrder::where('status_id', 4)
                    ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;

            case 'cancelled':
                $queryBuilder = PurchaseOrder::where('status_id', 5)
                    ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;

            case 'received':
                $queryBuilder = PurchaseOrder::where('status_id', 6)
                    ->whereBetween(DB::raw('DATE(created_at)'), [$startDate, $endDate])
                    ->latest('created_at');
                break;
        }

        $columns = [
            'Purchase Order#' => function ($po) {
                return $po->getPurchaseOrderNo();
            },
            'Supplier' => function ($po) {
                return $po->supplier->name;
            },
            'Description' => function ($po) {
                return $po->description;
            },
            'Date Ordered' => function ($po) {
                return $po->created_at;
            }
        ];

        return PdfReport::of($title, $meta, $queryBuilder, $columns)
            ->editColumn('Date Ordered', [
                'displayAs' => function ($po) {
                    return $po->created_at->format('M d, Y');
                }
            ])
            ->download($fileName);
    }
}
