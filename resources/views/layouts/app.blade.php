<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name') }}</title>

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Fontawesome Css -->
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">

    <!-- bootstrap-select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{ asset('css/frontend/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend/additional.css') }}" rel="stylesheet">

    <!-- Additional Css -->
    @yield('styles')
</head>
<body>
    @include('includes.frontend.header')

    @yield('content')

    @include('includes.frontend.footer')
        
    @yield('scripts')
</body>
</html>
