<?php

use Carbon\Carbon;
use App\Subcategory;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        Subcategory::insert([
            [
                'category_id' => 1,
                'name' => 'Batteries',
                'slug' => 'batteries',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 1,
                'name' => 'Wire',
                'slug' => 'wire',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 2,
                'name' => 'Adhesives',
                'slug' => 'adhesives',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 2,
                'name' => 'Tapes',
                'slug' => 'tapes',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 3,
                'name' => 'Coco Lumber',
                'slug' => 'coco-lumber',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 3,
                'name' => 'PlyWood',
                'slug' => 'plywood',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 4,
                'name' => 'Paints',
                'slug' => 'paints',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 4,
                'name' => 'Thinner',
                'slug' => 'thinner',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 5,
                'name' => 'Accessories',
                'slug' => 'accessories',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 5,
                'name' => 'Valves',
                'slug' => 'valves',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 6,
                'name' => 'Roof',
                'slug' => 'roof',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 6,
                'name' => 'Sealant',
                'slug' => 'sealant',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 7,
                'name' => 'Hand Tools',
                'slug' => 'hand-tools',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'category_id' => 7,
                'name' => 'Power Tools',
                'slug' => 'power-tools',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
