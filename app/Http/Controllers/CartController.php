<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('customer');
    }

    public function index()
    {
        return view('frontend.cart');
    }

    public function store(Request $request)
    {
        $product = Product::findOrFail($request->id);

        if ($product->isOutOfStock()) {
            return redirect()->back()->with('error', 'Sorry, this product is out of stock');
        }

        Cart::add($product, 1);

        return redirect()->back()->with('success', 'Product added to cart');
    }

    public function update(Request $request)
    {
        $item = Cart::get($request->rowId);

        if ($request->qty > $item->model->quantity) {
            return response()->json([
                'error' => "Product <b><i>({$item->model->name})</i></b> has <b><i>{$item->model->quantity}</i></b> remaining stocks",
                'qty' => $item->qty
            ]);
        }

        Cart::update($request->rowId, $request->qty);
        return response()->json([
            'success' => 'Cart successfully saved',
            'total' => number_format($item->price * $request->qty, 2),
            'subtotal' => Cart::subtotal(),
            'subtotalParse' => Cart::subtotal(2, '.', ''),
            'vat' => Cart::tax()
        ]);
    }

    public function destroy($rowId)
    {
        Cart::remove($rowId);
        return redirect()->back();
    }

    public function empty()
    {
        Cart::destroy();
        return redirect()->back()->with('success', 'Items have been removed from cart');
    }
}
