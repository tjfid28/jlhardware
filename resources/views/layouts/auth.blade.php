<!DOCTYPE html>
<html>

@include('includes.auth.header')

@php
    $classPage = null;
    $classBox = null;

    if (Request::is('login')) {
        $classPage = 'login-page';
        $classBox = 'login-box';
    } else if (Request::is('register')) {
        $classPage = 'signup-page';
        $classBox = 'signup-box';
    } else if (Request::is('password/reset')) {
        $classPage = 'fp-page';
        $classBox = 'fp-box';
    } else {
        $classPage = 'fp-page';
        $classBox = 'fp-box';
    }
@endphp


<body class="{{ $classPage }}">
    <div class="{{ $classBox }}">
        <div class="logo">
            <a href="javascript:void(0);">JL<b>Hardware</b></a>
            <small>Your best hardware in town</small>
        </div>

        <div class="card">
            <div class="body">
                @yield('content')
            </div>
        </div>
    </div>

    @include('includes.auth.script')
</body>

</html>
