setRow = ({ rowId, id, name, price, qty }, maxQuantity)=> (`
    <tr class="row${id}" data-rowid=${rowId} data-id="${id}">
        <td class="text-center"><strong>${name}</strong></td>
        <td class="text-center">${parseFloat(price).format(2)}</td>
        <td class="text-center">
            <div data-trigger="spinner" class="form-inline spinner">
                <button type="button" class="btn btn-default waves-effect" data-spin="down">-</button>
                <input name="qty" type="text" class="form-control qty qty${id}" value="${qty}" data-rule="quantity" data-max="${maxQuantity}">
                <button type="button" class="btn btn-default waves-effect" data-spin="up">+</button>
            </div>
        </td>
        <td class="text-center total total${id}">
            ${parseFloat(price * qty).format(2)}
        </td>
        <td class="text-center">
            <button type="button" data-id="${id}" data-rowid="${rowId}" data-name="${name}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="remove">
                <i class="material-icons">close</i>
            </button>
        </td>
    </tr>
`);

setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

spin = () => {
    $(".spinner")
        .spinner('delay', 0) //delay in ms
        .spinner('changed', function(e, newVal, oldVal) {
            // trigger lazed, depend on delay option.
            const id = $(this).closest('tr').data('id');
            const rowId = $(this).closest('tr').data('rowid');

            $.ajax({
                url: `/orders/update-item/${id}`,
                type: 'PATCH',
                data: { id, rowId, qty: newVal },
                success: response => {
                    if (response.success) {
                        $(this).closest('tr').find('.total').text(response.total);
                        $('.subtotal').text(response.subtotal);
                        $('.vat').text(response.vat);
                        $('.amount-due').text(response.subtotal);
                    } else {
                        $(this)[0].value = response.qty;
                        iziToast.error({
                            title: 'Error',
                            message: response.error
                        });
                    }
                },
                error: response => console.log(response),
            });
        })
        .spinner('changing', function(e, newVal, oldVal) {
            // trigger immediately
        });
}

$(document).ready(() => {
    /* Variable declaration - initialization */
    const itemsTable = $(document).find('.items-table');
    const addForm = $(document).find('.addForm');
    const checkoutForm = $(document).find('.form-checkout');
    const deleteForm = $(document).find('.deleteForm');
    const deleteModal = $(document).find('.deleteModal');

    const dt = itemsTable.DataTable({
        "ordering": false,
        "columnDefs": [
            { "searchable": false, "targets": [1, 2, 3, 4] }
        ],
        "columns": [
            { "width": "20%" },
            { "width": "15%" },
            { "width": "40%" },
            { "width": "15%" },
            { "width": "10%" }
        ]
    });

    spin();

    addForm.validate({
        rules: {
            product: {
                required: true
            },
            qty: {
                required: true,
                digits: true,
                min: 1
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    addForm.submit(event => {
        event.preventDefault();

        if (addForm.valid()) {
            const product = $('.add-product').selectpicker('val');
            const qty = $('.add-qty').val();

            $.ajax({
                url: addForm.attr('action'),
                type: addForm.attr('method'),
                beforeSend: () => {
                    $('.btnSave').html('<i class="fa fa-spinner fa-spin"></i> Saving...');
                },
                data: { product, qty },
                success: response => {
                    if (response.success) {
                        const { id, price, qty } = response.data;
                        const maxQuantity = response.maxQuantity;

                        if (!$(`.row${id}`).length) {
                            const row = setRow(response.data, maxQuantity);
                            dt.row.add($(row)).draw(false);
                        } else {
                            $(`.qty${id}`).val(parseFloat(qty));
                            $(`.qty${id}`).data('max', parseFloat(maxQuantity));
                            $(`.total${id}`).text(parseFloat(price * qty).format(2));
                        }

                        $('.spinner').spinner('destroy');
                        spin();
                        
                        if (parseFloat(response.count) === 1) {
                            $('.quotations').removeClass('hidden');
                            $('.action-buttons').removeClass('hidden');
                        }

                        $('.subtotal').text(response.subtotal);
                        $('.vat').text(response.vat);
                        $('.amount-due').text(response.subtotal);

                        addForm.trigger("reset");
                        $('.add-product').selectpicker('refresh');

                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.warning({
                            title: 'Warning',
                            message: response.error,
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnSave').html('SAVE')
            });
        }
    });

    deleteModal.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const rowId = e.relatedTarget.dataset.rowid;
        const name = e.relatedTarget.dataset.name;

        deleteForm.attr('action', `/orders/delete-item/${rowId}`);
        $('.delete-id').val(id);
        $('.delete-name').val(name);
    });

    deleteForm.submit(event => {
        event.preventDefault();
        const _method = deleteForm.attr('method');
        const id = $('.delete-id').val();

        $.ajax({
            url: deleteForm.attr('action'),
            type: _method,
            beforeSend: () => {
                $('.btnDelete').html('<i class="fa fa-spinner fa-spin"></i> Deleting...');
            },
            data: { _method },
            success: response => {
                if (response.success) {
                    dt.row(`.row${id}`).remove().draw(false);

                    if (parseFloat(response.count) === 0) {
                        $('.quotations').addClass('hidden');
                        $('.action-buttons').addClass('hidden');
                    }

                    $('.subtotal').text(response.subtotal);
                    $('.vat').text(response.vat);
                    $('.amount-due').text(response.subtotal);

                    deleteModal.modal('hide');
                    iziToast.success({
                        title: 'Success',
                        message: response.success
                    });
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: response.error
                    });
                }
            },
            error: response => console.log(response),
            complete: () => $('.btnDelete').html('DELETE')
        });
    });

    checkoutForm.validate({
        rules: {
            name: {
                required: true,
                normalizer: value => $.trim(value)
            },
            address: {
                required: true,
                normalizer: value => $.trim(value)
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });
});
