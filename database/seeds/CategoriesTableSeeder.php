<?php

use App\Category;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        Category::insert([
            [
                'name' => 'Electrical',
                'slug' => 'electrical',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Hardware',
                'slug' => 'hardware',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Lumber',
                'slug' => 'lumber',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Paints and Sundries',
                'slug' => 'paints-and-sundries',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Plumbing',
                'slug' => 'plumbing',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Roofing',
                'slug' => 'roofing',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Tools',
                'slug' => 'tools',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
