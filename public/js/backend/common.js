/* Formating number */
Number.prototype.format = function(n, x) {
    const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

$(document).ready(() => {

    /* Ajax Setup for CSRF Token */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    /* Tooltip */
    $('body').tooltip({
        selector: '[data-tooltip="tooltip"]',
        trigger : 'hover',
        container: 'body'
    });


    /* Izitoast */
    try {
        iziToast.settings({
            position: 'topRight'
        });
    } catch (e) { }

});
