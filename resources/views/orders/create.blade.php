@extends('layouts.backend')

@section('title', 'Create Order')

@section('styles')
    <!-- JQuery dataTables CSS -->
    <link href="{{ asset('css/backend/datatables.min.css') }}" rel="stylesheet">

    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- bootstrap-select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">

    <!-- jQuery Spinner CSS -->
    <link href="{{ asset('css/frontend/bootstrap-spinner.min.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        CREATE ORDER
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-xs-6">
                            <h2>List of Items</h2>
                        </div>

                        <div class="col-md-6 col-xs-6 text-right">
                            <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".addModal">
                                <i class="material-icons">add</i>
                                <span>Add Item</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="body">
                    @include('includes.message')

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped items-table">
                            <thead>
                                <tr>
                                    <th class="text-center">Product</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse (Cart::content() as $item)
                                    <tr class="row{{ $item->id }}" data-rowid={{ $item->rowId }} data-id="{{ $item->id }}">
                                        <td class="text-center"><strong>{{ $item->name }}</strong></td>
                                        <td class="text-center">{{ number_format($item->price, 2) }}</td>
                                        <td class="text-center">
                                            <div data-trigger="spinner" class="form-inline spinner">
                                                <button type="button" class="btn btn-default waves-effect" data-spin="down">-</button>
                                                <input name="qty" type="text" class="form-control qty qty{{ $item->id }}" value="{{ $item->qty }}" data-rule="quantity" data-max="{{ $item->model->quantity }}">
                                                <button type="button" class="btn btn-default waves-effect" data-spin="up">+</button>
                                            </div>
                                        </td>
                                        <td class="text-center total total{{ $item->id }}">
                                            {{ number_format($item->price * $item->qty, 2) }}
                                        </td>
                                        <td class="text-center">
                                            <button type="button" data-id={{ $item->id }} data-rowid="{{ $item->rowId }}" data-name="{{ $item->name }}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="remove">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>

                        <div class="col-md-6">
                            <table class="table table-bordered table-striped quotations {{ !Cart::content()->count() ? 'hidden' : '' }}">
                                <tbody>
                                    <tr>
                                        <td class="text-right"><strong>Sub Total</strong></td>
                                        <td class="text-primary subtotal">{{ Cart::subtotal() }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>VAT ({{ config('cart.tax') }}%)</strong></td>
                                        <td class="text-danger vat">{{ Cart::tax() }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>Amount Due</strong></td>
                                        <td class="text-danger amount-due"><strong>{{ Cart::subtotal() }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6 action-buttons {{ !Cart::content()->count() ? 'hidden' : '' }}">
                            <button class="btn btn-danger btn-lg" data-toggle="modal" data-target=".emptyModal">Empty Cart</button>
                            <button class="btn btn-success btn-lg" data-toggle="modal" data-target=".checkoutModal">Check Out</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade addModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Add item</h4>
                </div>
                <form class="addForm" action="{{ route('orders.addItem') }}" method="POST">
                    <div class="modal-body">
                        <label for="product">Product*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control add-product selectpicker show-tick" name="product" data-live-search="true">
                                    <option value="">-- Please select a product --</option>
                                    @forelse ($products as $product)
                                        <option value="{{ $product->id }}" data-tokens="{{ $product->name }}">
                                            {{ $product->name }} ({{ $product->quantity }})
                                        </option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control add-qty" name="qty">
                                <label class="form-label">Quantity*</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnSave waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Remove product from cart</h4>
                </div>
                <form class="deleteForm" action="" method="DELETE">
                    <div class="modal-body">
                        <h5>Are you sure you want to remove this product?</h5>
                        <input class="delete-id" type="hidden" name="id">
                        
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control delete-name" type="text" value="" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btnDelete">REMOVE</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade emptyModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('orders.empty') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <h4>Are you sure you want to remove all items from cart?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade checkoutModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">
                        <i class="fa fa-shopping-cart text-success fa-lg"></i> Check Out
                        <small class='text-primary'> Customer Information</small>
                    </h4>
                </div>
                <form action="{{ route('orders.checkout') }}" method="POST" class="form-checkout">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-name" name="name">
                                <label class="form-label">Name*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize add-address" name="address"></textarea>
                                <label class="form-label">Address</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- JQuery dataTables JS -->
    <script type="text/javascript" src="{{ asset('js/backend/datatables.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- bootstrap-select Js -->
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>

    <!-- Spinner Plugin Js -->
    <script src="{{ asset('js/frontend/jquery.spinner.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/create-order.js') }}"></script>
@endsection
