<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->unsignedInteger('supplier_id')->nullable(false);
            $table->foreign('supplier_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('status_id')->nullable(false);
            $table->foreign('status_id')->references('id')->on('status')->onDelete('cascade');
            $table->text('description')->nullable(false);
            $table->timestamps();
        });

        DB::update("ALTER TABLE purchase_orders AUTO_INCREMENT = 10000000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
