<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class BrandsController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff')->except('show');
    }

    public function index()
    {
        $brands = Brand::latest()->get();
        return view('brands.index', compact('brands'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:brands'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $brand = Brand::create([
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description
        ]);

        return response()->json(['success' => 'Brand successfully saved', 'data' => $brand]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('brands')->ignore($request->id)
            ]
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $brand = Brand::find($request->id);
        $brand->name = $request->name;
        $brand->slug = str_slug($request->name);
        $brand->description = $request->description;
        $brand->save();

        return response()->json(['success' => 'Brand successfully updated', 'data' => $brand]);
    }

    public function destroy($id)
    {
        try {
            Brand::destroy($id);
            return response()->json(['success' => 'Brand successfully deleted']);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        };
    }

    public function show($slug)
    {
        if (strcmp($slug, "all") === 0) {
            $brand = (object) ['name' => 'All', 'slug' => 'all'];
            $products = Product::latest('updated_at')->paginate(12);
        } else {
            $brand = Brand::where('slug', $slug)->firstOrFail();
            $products = Product::where('brand_id', $brand->id)->latest('updated_at')->paginate(12);
        }
        
        return view('brands.show', compact('products', 'brand'));
    }
}
