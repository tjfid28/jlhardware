<?php

use App\Order;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        Order::insert([
            [
                'user_id' => 3,
                'status_id' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 3,
                'status_id' => 2,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 3,
                'status_id' => 3,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 3,
                'status_id' => 4,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'user_id' => 3,
                'status_id' => 5,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
