<?php

use App\Slide;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SlidesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Slide::insert([
            [
                'image' => 'slide1.jpg',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'image' => 'slide2.jpg',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'image' => 'slide3.jpg',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
