$(document).ready(() => {
    $('form').validate({
        rules: {
            email: {
                required: true,
                email: true,
                normalizer: value => $.trim(value)
            }, 
            password: {
                required: true,
                minlength: 6,
                normalizer: value => $.trim(value)
            },
            password_confirmation: {
                required: true,
                minlength: 6,
                equalTo: '.password'
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        }
    });
});
