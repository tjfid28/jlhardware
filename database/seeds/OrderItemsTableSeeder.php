<?php

use App\OrderItem;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        OrderItem::insert([
            [
                'order_id' => 1,
                'product_id' => 1,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 1,
                'product_id' => 2,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 2,
                'product_id' => 3,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 2,
                'product_id' => 4,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 3,
                'product_id' => 5,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 3,
                'product_id' => 6,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 4,
                'product_id' => 7,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 4,
                'product_id' => 8,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 5,
                'product_id' => 9,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'order_id' => 5,
                'product_id' => 10,
                'quantity' => $faker->numberBetween(1, 5),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
