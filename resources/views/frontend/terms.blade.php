@extends('layouts.app')

@section('title', 'Terms and Conditions')

@section('content')
    <section class="bgimage bg-imageURL-terms">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Terms &amp; Conditions</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="default-margin">
        <div class="container">
            <div class="row">
                <div id="id-terms" class="col-md-12">
                    <ol>
                        @foreach ($terms as $term)
                            <li>{{ $term->description }}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </section>
@endsection
