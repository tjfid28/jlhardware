@extends('layouts.app')

@section('title', 'My Purchase')

@section('styles')
    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <section class="bg-texture">
        <div class="container" style="padding-top: 20px">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <b>Note: </b> Please refresh this page to see the latest update of your orders.
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default" style="padding: 10px">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-justified" id="myTabs" role="tablist" style="font-size: 16px">
                            @forelse ($status as $key => $value)
                                @if ($key !== 2)
                                    <li role="presentation" class="{{ $key === 0 ? 'active' : '' }}">
                                        <a href="#{{ $key === 1 ? 'for-shipping' : $value->getSafeName() }}" aria-controls="{{ $key === 1 ? 'for-shipping' : $value->getSafeName() }}" role="tab" data-toggle="tab">
                                            <strong>{{ $key === 1 ? 'For Shipping' : $value->name }}</strong>
                                        </a>
                                    </li>
                                @endif
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <!-- Tab panes -->
                            <div class="container-fluid" style="padding-top: 15px">
                                <div class="tab-content">
                                    @forelse ($status as $key => $value)
                                        @if ($key !== 2)
                                            <div role="tabpanel" class="tab-pane {{ $key === 0 ? 'active fade in' : 'fade' }}" id="{{ $key === 1 ? 'for-shipping' : $value->getSafeName() }}">
                                                <div class="panel-group" id="accordion{{ $key }}" role="tablist" aria-multiselectable="true">
                                                    @php
                                                        $filtered = $key === 1 ? $orders->whereIn('status_id', [2, 3]) : $orders->where('status_id', $value->id);
                                                    @endphp

                                                    @forelse ($filtered->all() as $oKey => $order)
                                                        @php
                                                            $total = 0;
                                                        @endphp

                                                        <div class="panel panel-default" style="margin-bottom: 15px">
                                                            <div class="panel-heading" role="tab" id="heading{{ $order->id }}">
                                                                <h4 class="panel-title">
                                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion{{ $key }}" href="#collapse{{ $order->id }}" aria-expanded="false" aria-controls="collapse{{ $order->id }}">
                                                                        <div>{{ $order->getOrderNumber() }}</div>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                        </div>

                                                        <div id="collapse{{ $order->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $order->id }}">
                                                            <div class="panel-body">
                                                                <div class="list-group">
                                                                    @foreach ($order->orderItems as $item)
                                                                        @php
                                                                            $total += $item->product->price * $item->quantity
                                                                        @endphp

                                                                        <input type="hidden" value="{{ $item->product }}">
                                                                        <a href="{{ route('products.show', $item->product->slug) }}" class="list-group-item" style="display: flex; align-items: center">
                                                                            <img src="{{ $item->product->getImageUrl() }}" height="80" width="80">
                                                                            <div style="margin-left: 15px">
                                                                                <h4>{{ $item->product->name }}</h4>
                                                                                <span>x {{ $item->quantity }}</span>
                                                                            </div>
                                                                            <h4 style="margin-left: auto">
                                                                                Php. {{ number_format($item->quantity * $item->product->price, 2) }}
                                                                            </h4>
                                                                        </a>
                                                                        @if ($order->status->isPending())
                                                                            <h4 class="text-right">
                                                                                <button type="button" class="btn btn-danger btn-sm" data-id="{{ $item->id }}" data-name="{{ $item->product->name }}" data-toggle="modal" data-target=".cancel-item-modal">
                                                                                    Cancel
                                                                                </button>
                                                                            </h4>
                                                                        @endif
                                                                    @endforeach
                                                                </div>

                                                                <div class="text-right">
                                                                    <h3>
                                                                        Order Total:
                                                                        <b style="color: #f6960d">Php. {{ number_format($total, 2) }}</b>
                                                                    </h3>

                                                                    @if ($order->status->isPending())
                                                                        <button type="button" class="btn btn-danger btn-lg" data-id="{{ $order->id }}" data-toggle="modal" data-target=".cancelModal">
                                                                            Cancel All
                                                                        </button>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @php
                                                            $total = 0;
                                                        @endphp
                                                    @empty
                                                        <div style="height: 200px; display: flex; align-items: center; justify-content: center;">
                                                            <h3>No orders yet</h3>
                                                        </div>
                                                    @endforelse
                                                </div>
                                            </div>
                                        @endif
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Cancel order modal -->
    <div class="modal fade cancelModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Are you sure you want to cancel order <b><i></i></b>?</h4>
                </div>
                <form action="{{ route('user.cancelOrder') }}" method="POST">
                    @csrf
                    <input type="hidden" class="cancel-id" name="id" value="">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Cancel order modal -->

    <!-- Cancel Item modal -->
    <div class="modal fade cancel-item-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="cancel-item-modal-header"></h4>
                </div>
                <form action="{{ route('user.cancelOrderItem') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" class="cancel-item-id" name="id" value="">

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Cancel Item modal -->
@endsection

@section('scripts')
    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/frontend/orders.js') }}"></script>

    <script>
        $(document).ready(() => {
            @if (Session::has('success'))
                iziToast.success({
                    title: 'Success',
                    message: "{!! Session::get('success') !!}",
                    position: 'topRight'
                });
            @endif

            @if (Session::has('warning'))
                iziToast.warning({
                    title: 'Warning',
                    message: "{!! Session::get('warning') !!}",
                    position: 'topRight',
                    timeout: 10000
                });
            @endif
        });
    </script>
@endsection
