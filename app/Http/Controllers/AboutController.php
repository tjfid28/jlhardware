<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff')->except('index');
    }

    public function index()
    {
        $about = About::find(1);
        return view('about.index', compact('about'));
    }

    public function edit()
    {
        $about = About::find(1);
        return view('about.edit', compact('about'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $about = About::find(1);
        $about->content = $request->content;
        $about->save();

        return response()->json(['success' => 'About successfully updated', 'data' => $about]);
    }
}
