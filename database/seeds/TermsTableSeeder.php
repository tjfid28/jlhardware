<?php

use App\Term;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Term::insert([
            [
                'description' => 'If you order online, you should pay us 100% on cash on delivery or you can go to the store to check the items or to pay us by cash on hand.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'You can cancel your order but make sure that you will tell to the owner when he/she call you to inform you that your order is ready and about to deliver.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'Access to and use of password protected and/or secure areas of the platform and use of the services are restricted to customers with accounts only.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'Username/ Password: certain services that may be made available on the platform may require creation of an account with us or for you to provide personal data. If you request to create an account with us, a username and password may either be: (i) determined and issued to you by us; (ii) provided by you and accepted by us.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'Not to impersonate any person or entity or to falsely state or otherwise misrepresent your affiliation with any person or entity. Not to use this platform or services for illegal purposes.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'The delivery men will check and discuss all of the items to the customer after they get rid of it to the vehicle just in case the items have any damage caused of delivery or in factory defect.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'Allows returning of items just in case of any damage, factory defect or etc. No refund is available in the event of cancellation at any point when payment is by cash on hand.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'The delivery process of Johnlyntin Hardware is that, they only deliver from North Caloocan to Quezon City and they deliver the items for free as long as the location of the customer is inside the North Caloocan City. If along Quezon City, the delivery fee will depend on the customer location. And an estimated delivery time will be provided to you when the order is placed.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'Store Pickup. The customer can pick up immediately the item(s) after it is already paid to the cashier of the hardware. As a result, risk of loss and a little for such products pass to you when you pick up the products from the designated Johnlyntin Hardware store.',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'description' => 'Same Day Delivery. You may have the product/s delivered by Johnlyntin Hardware associate to an address designated by you, as long as that delivery address is compliant with our applicable same day delivery restrictions but depends on how many deliveries we have in a day to apply that same day delivery restrictions. You must be present in order to accept and sign for the delivery.',
                'created_at' => $now,
                'updated_at' => $now
            ],
        ]);
    }
}
