setRow = ({ id, brand_id, category_id, subcategory_id, supplier_id, name, price, quantity, description, image, created_at, updated_at, brand, category, subcategory, supplier })=> (`
    <tr class="row${id} ${parseInt(quantity) === 0 ? 'bg-grey' : parseInt(quantity) <= 10 ? 'bg-red' : ''}">
        <td class="btnActionContainer">
            <button type="button" data-id="${id}" class="btn btn-success btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".editModal" title="edit">
                <i class="material-icons">edit</i>
            </button>
            &nbsp;
            <button type="button" data-id="${id}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                <i class="material-icons">delete</i>
            </button>
        </td>
        <td class="image-${id}">
            <a href="${image}" target="_blank">
                <img src="${image}" alt="${image}" width="48" height="24">
            </a>
        </td>
        <td class="name-${id}">${name}</td>
        <td class="brand_id-${id}">${brand.id}</td>
        <td class="brand_name-${id}">${brand.name}</td>
        <td class="category-${id}">${category.name}</td>
        <td class="subcategory_id-${id}">${subcategory.id}</td>
        <td class="subcategory_name-${id}">${subcategory.name}</td>
        <td class="supplier_id-${id}">${supplier.id}</td>
        <td class="supplier_name-${id}">${supplier.name}</td>
        <td class="price-${id}" data-price="${price}">${parseFloat(price).format(2)}</td>
        <td class="quantity-${id}">${quantity}</td>
        <td class="description-${id}">${description ? description : ''}</td>
        <td data-order="${created_at}">${created_at}</td>
        <td class="updated_at-${id}" data-order="${updated_at}">${updated_at}</td>
    </tr>
`);

setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

$(document).ready(() => {

    /* Variable declaration - initialization */
    const table = $(document).find('table');
    const addForm = $(document).find('.addForm');
    const addModal = $(document).find('.addModal');
    const editForm = $(document).find('.editForm');
    const editModal = $(document).find('.editModal');
    const deleteForm = $(document).find('.deleteForm');
    const deleteModal = $(document).find('.deleteModal');

    const dt = table.DataTable({
        initComplete: function () {
            this.api().columns([4, 5, 7, 9]).every( function () {
                let column = this;
                let select = $(`<select><option value="">${$(this.header()).text()}</option></select>`)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        let val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
        order: [[14, "desc"]],
        "columnDefs": [
            { "orderable": false, "targets": [0, 1, 12] },
            { "searchable": false, "targets": [0, 1, 3, 6, 8] },
            { "className": "hidden", "targets": [3, 6, 8] }
        ],
        scrollX: true
    });

    $.validator.addMethod('decimal', function(value, element) {
        return this.optional(element) || /^[0-9]*\.[0-9]{2}$/.test(value);
    }, "Please enter a correct price , format 0.00");

    addForm.validate({
        rules: {
            image: {
                required: true,
                accept: "image/jpg,image/jpeg,image/png"
            },
            name: {
                required: true,
                normalizer: value => $.trim(value)
            },
            brand_id: {
                required: true
            },
            subcategory_id: {
                required: true
            },
            supplier_id: {
                required: true
            },
            price: {
                required: true,
                decimal: true
            },
            quantity: {
                required: true,
                min: 0
            },
            description: {
                normalizer: value => $.trim(value)
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    addForm.submit(event => {
        event.preventDefault();

        if (addForm.valid()) {
            const formData = new FormData(addForm[0]);

            $.ajax({
                url: addForm.attr('action'),
                type: addForm.attr('method'),
                beforeSend: () => {
                    $('.btnSave').html('<i class="fa fa-spinner fa-spin"></i> Saving...');
                },
                processData: false,
                contentType: false,
                data: formData,
                success: response => {
                    if (response.success) {
                        const row = setRow(response.data);

                        dt.row.add($(row)).draw(false);
                        addForm.trigger("reset");
                        $('.add-brand').selectpicker('refresh');
                        $('.add-subcategory').selectpicker('refresh');
                        $('.add-supplier').selectpicker('refresh');

                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnSave').html('SAVE')
            });
        }
    });

    editForm.validate({
        rules: {
            image: {
                accept: "image/jpg,image/jpeg,image/png"
            },
            name: {
                required: true,
                normalizer: value => $.trim(value)
            },
            brand_id: {
                required: true
            },
            subcategory_id: {
                required: true
            },
            supplier_id: {
                required: true
            },
            price: {
                required: true,
                decimal: true
            },
            quantity: {
                required: true,
                min: 0
            },
            description: {
                normalizer: value => $.trim(value)
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    editModal.on('shown.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const name = $(`.name-${id}`).text();
        const brand_id = $(`.brand_id-${id}`).text();
        const subcategory_id = $(`.subcategory_id-${id}`).text();
        const supplier_id = $(`.supplier_id-${id}`).text();
        const price = $(`.price-${id}`).data('price');
        const quantity = $(`.quantity-${id}`).text();
        const description = $(`.description-${id}`).text();

        $('.edit-id').val(id);
        $('.edit-description').val(description ? description : '').focus();
        $('.edit-quantity').val(quantity).focus();
        $('.edit-price').val(price).focus();
        $('.edit-supplier').selectpicker('val', supplier_id);
        $('.edit-subcategory').selectpicker('val', subcategory_id);
        $('.edit-brand').selectpicker('val', brand_id);
        $('.edit-name').val(name).focus();
    });

    editForm.submit(event => {
        event.preventDefault();

        if (editForm.valid()) {
            const _method = editForm.attr('method');
            const formData = new FormData(editForm[0]);
            formData.append('_method', _method);
            const id = formData.get('id');

            $.ajax({
                url: `/products/${id}`,
                type: 'POST',
                beforeSend: () => {
                    $('.btnUpdate').html('<i class="fa fa-spinner fa-spin"></i> Updating...');
                },
                processData: false,
                contentType: false,
                data: formData,
                success: response => {
                    if (response.success) {
                        const { brand_id, subcategory_id, supplier_id, name, price, quantity, description, image, updated_at, brand, category, subcategory, supplier } = response.data;
                        const tempRow = dt.row(`.row${id}`).data();
                        tempRow[2] = name;
                        tempRow[3] = brand_id;
                        tempRow[4] = brand.name;
                        tempRow[5] = category.name;
                        tempRow[6] = subcategory_id;
                        tempRow[7] = subcategory.name;
                        tempRow[8] = supplier_id;
                        tempRow[9] = supplier.name;
                        tempRow[10] = parseFloat(price).format(2);
                        tempRow[11] = quantity;
                        tempRow[12] = description;
                        tempRow[14].display = updated_at;
                        $(`.price-${id}`).attr('data-price', price);
                        $(`.updated_at-${id}`).attr('data-order', updated_at);

                        $('.edit-image').val('');
                        dt.row(`.row${id}`).data(tempRow).invalidate().draw(false);

                        if (parseInt(quantity) === 0) {
                            $(`.row${id}`).attr('class', `row${id} bg-grey`);
                        } else {
                            if (parseInt(quantity) <= parseInt($('.critical_level').val())) {
                                $(`.row${id}`).attr('class', `row${id} bg-red`);
                            } else {
                                $(`.row${id}`).attr('class', `row${id}`);
                            }
                        }

                        $(`.image-${id}`).find('a').attr('href', image);
                        $(`.image-${id}`).find('img').attr('src', image);
                        $(`.image-${id}`).find('img').attr('alt', image);
                        dt.draw(false);

                        editModal.modal('hide');
                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnUpdate').html('UPDATE')
            });
        }
    });

    deleteModal.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const image = $(`.image-${id}`).find('img').attr('src');
        const name = $(`.name-${id}`).text();
        const brand_name = $(`.brand_name-${id}`).text();
        const category = $(`.category-${id}`).text();
        const subcategory_name = $(`.subcategory_name-${id}`).text();
        const supplier_name = $(`.supplier_name-${id}`).text();
        const price = $(`.price-${id}`).text();
        const quantity = $(`.quantity-${id}`).text();
        const description = $(`.description-${id}`).text();

        $('.delete-id').val(id);
        $('.delete-image').attr('src', image);
        $('.delete-name').val(name);
        $('.delete-brand').val(brand_name);
        $('.delete-category').val(category);
        $('.delete-subcategory').val(subcategory_name);
        $('.delete-supplier').val(supplier_name);
        $('.delete-price').val(price);
        $('.delete-quantity').val(quantity);
        $('.delete-description').val(description ? description : '');
    });

    deleteForm.submit(event => {
        event.preventDefault();
        const _method = deleteForm.attr('method');
        const id = $('.delete-id').val();

        $.ajax({
            url: `/products/${id}`,
            type: _method,
            beforeSend: () => {
                $('.btnDelete').html('<i class="fa fa-spinner fa-spin"></i> Deleting...');
            },
            data: { _method },
            success: response => {
                if (response.success) {
                    deleteModal.modal('hide');
                    iziToast.success({
                        title: 'Success',
                        message: response.success
                    });
                    dt.row(`.row${id}`).remove().draw(false);
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: setErrors(response.error),
                        timeout: false
                    });
                }
            },
            error: response => console.log(response),
            complete: () => $('.btnDelete').html('DELETE')
        });
    });

});
