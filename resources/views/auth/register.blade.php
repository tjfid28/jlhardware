@extends('layouts.auth')

@section('title', 'Register')

@section('content')

<div style="display: flex;align-items: center;justify-content: space-between;">
    <h3>Register a new membership</h3>
    <div>
        <a href="{{ route('login') }}">You already have a membership?</a>
    </div>
</div>

@include('includes.message')

<form class="m-t-30" id="wizard_with_validation" method="POST" action="{{ route('register') }}">
    @csrf

    <h3>Account Information</h3>
    <fieldset>
        <div class="form-group form-float">
            <div class="form-line">
                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                <label class="form-label">Name*</label>
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                <label class="form-label">Email*</label>
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                <input type="password" class="form-control" name="password" id="password">
                <label class="form-label">Password*</label>
            </div>
        </div>
        <div class="form-group form-float">
            <div class="form-line">
                <input type="password" class="form-control" name="password_confirmation">
                <label class="form-label">Confirm Password*</label>
            </div>
        </div>
    </fieldset>

    <h3>Billing Information</h3>
    <fieldset>
        <div class="row clearfix">
            <div class="col-md-6 col-xs-12">
                <b>Mobile No.*</b>
                <div class="form-group masked-input input-group">
                    <span class="input-group-addon">
                        +63
                    </span>
                    <div class="form-line">
                        <input type="number" class="form-control mobile-no" placeholder="Ex. 9876543210" name="mobile_no" value="{{ old('mobile_no') }}">
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xs-12">
                <b>Telephone No. <i>(optional)</i></b>
                <div class="form-group masked-input input-group">
                    <span class="input-group-addon">
                        (02)
                    </span>
                    <div class="form-line">
                        <input type="number" class="form-control telephone-no" placeholder="Ex. 1234567" name="telephone_no" value="{{ old('telephone_no') }}">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row clearfix">
            <h4 style="margin-top: -30px" class="col-md-12">Address</h4>
            <div class="form-group form-float col-md-12">
                <div class="form-line">
                    <input type="text" class="form-control" name="building_street_info" value="{{ old('building_street_info') }}">
                    <label class="form-label">Bldg. / Street / Subd.*</label>
                </div>
            </div>
        </div>
        
        <div class="row clearfix">
            <div class="col-md-6 col-xs-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="barangay" value="{{ old('barangay') }}">
                        <label class="form-label">Barangay*</label>
                    </div>
                </div>
            </div>
            

            <div class="col-md-6 col-xs-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="city" value="{{ old('city') }}">
                        <label class="form-label">City*</label>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="row clearfix">
            <div class="col-md-6 col-xs-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="province" value="{{ old('province') }}">
                        <label class="form-label">Province*</label>
                    </div>
                </div>
            </div>
            

            <div class="col-md-6 col-xs-12">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" class="form-control" name="region" value="{{ old('region') }}">
                        <label class="form-label">Region*</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="form-group form-float col-md-12">
                <div class="form-line">
                    <input type="text" class="form-control" name="postal_code" value="{{ old('postal_code') }}">
                    <label class="form-label">Postal code*</label>
                </div>
            </div>
        </div>
    </fieldset>

    <h3>Terms & Conditions - Finish</h3>
    <fieldset>
        <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
        <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
    </fieldset>
</form>
@endsection

@section('scripts')
    <script src="{{ asset('js/backend/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('js/backend/sign-up.js') }}"></script>
@endsection

