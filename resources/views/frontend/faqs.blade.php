@extends('layouts.app')

@section('title', 'Frequently Asked Questions')

@section('content')
    <section class="bgimage bg-imageURL-faqs">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2>Frequently Asked Questions</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="default-margin">
        <div class="container">
            <div class="row">
                <div id="id-faqs" class="col-md-12">
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        @foreach ($faqs as $key => $faq)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading{{ $key }}">
                                    <h3 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion{{ $key }}" href="#collapse{{ $key }}" aria-expanded="false" aria-controls="collapse{{ $key }}" class="collapsed">
                                            <span>{{ $faq->title }}</span>
                                        </a>
                                    </h3>
                                </div>

                                <div id="collapse{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $key }}">
                                    <div class="panel-body">{{ $faq->description }}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
