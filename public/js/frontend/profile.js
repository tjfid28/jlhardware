setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

$(document).ready(() => {

    /* Variable declaration - initialization */
    const editForm = $(document).find('.form-profile');

    editForm.validate({
        rules: {
            mobile_no: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10,
                normalizer: value => $.trim(value),
            },
            telephone_no: {
                number: true,
                minlength: 7,
                maxlength: 7,
                normalizer: value => $.trim(value),
            },
            building_street_info: {
                required: true,
                normalizer: value => $.trim(value)
            },
            barangay: {
                required: true,
                normalizer: value => $.trim(value)
            },
            city: {
                required: true,
                normalizer: value => $.trim(value)
            },
            province: {
                required: true,
                normalizer: value => $.trim(value)
            },
            region: {
                required: true,
                normalizer: value => $.trim(value)
            },
            postal_code: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 4,
                normalizer: value => $.trim(value)
            },
        },
        messages: {
            mobile_no : {
                minlength: "Please enter a valid mobile no.",
                maxlength: "Please enter a valid mobile no.",
            },
            telephone_no : {
                minlength: "Please enter a valid telephone no.",
                maxlength: "Please enter a valid telephone no.",
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    // editForm.submit(event => {
    //     event.preventDefault();

    //     if (editForm.valid()) {
    //         alert('yey');
    //         const _method = editForm.attr('method');
    //         const id = $('.edit-id').val();
    //         const name = $('.edit-name').val();
    //         const description = $('.edit-description').val();

    //         $.ajax({
    //             url: `/brands/${id}`,
    //             type: _method,
    //             beforeSend: () => {
    //                 $('.btnUpdate').html('<i class="fa fa-spinner fa-spin"></i> Updating...');
    //             },
    //             data: { _method, id, name, description },
    //             success: response => {
    //                 if (response.success) {
    //                     const tempRow = dt.row(`.row${id}`).data();
    //                     tempRow[1] = response.data.name;
    //                     tempRow[2] = response.data.description;
    //                     tempRow[4].display = response.data.updated_at;
    //                     $(`.updated_at-${id}`).attr('data-order', response.data.updated_at);

    //                     editModal.modal('hide');
    //                     iziToast.success({
    //                         title: 'Success',
    //                         message: response.success
    //                     });

    //                     dt.row(`.row${id}`).data(tempRow).invalidate().draw(false);
    //                 } else {
    //                     iziToast.error({
    //                         title: 'Error',
    //                         message: setErrors(response.errors),
    //                         timeout: false
    //                     });
    //                 }
    //             },
    //             error: response => console.log(response),
    //             complete: () => $('.btnUpdate').html('UPDATE')
    //         });
    //     }
    // });

});
