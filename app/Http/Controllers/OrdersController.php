<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('staff', ['except' => 'store']);
        $this->middleware('customer', ['only' => 'store']);
    }

    public function index()
    {
        $orders = Order::latest()->with(['orderItems', 'orderItems.product', 'orderItems.product.brand', 'orderItems.product.subcategory', 'orderItems.product.subcategory.category'])->get();
        return view('orders.index', compact('orders'));
    }

    public function store(Request $request)
    {
        $unavailableProducts = [];

        foreach (Cart::content() as $item) {
            if ($item->qty > $item->model->quantity) {
                $message = "<b>$item->name</b> has <b>{$item->model->quantity}</b> stocks left";
                $unavailableProducts[] = $message;
            }
        }

        if (count($unavailableProducts) > 0) {
            return redirect()->back()->with('customErrors', $unavailableProducts);
        }

        $order = Order::create([
            'user_id' => auth()->user()->id,
            'status_id' => 1
        ]);

        $orderItems = [];

        foreach (Cart::content() as $item) {
            $product = $item->model;
            $product->quantity = $product->quantity - $item->qty;
            $product->timestamps = false;
            $product->save();

            $orderItems[] = [
                'product_id' => $item->id,
                'quantity' => $item->qty
            ];
        }

        $order->orderItems()->createMany($orderItems);

        $admin = User::findOrFail(1);
        try {
            $admin->sendOrderPlacedEmail($order);
        } catch (Exception $e) {
        }

        Cart::destroy();
        return redirect()->back()->with('success', 'Order successfully saved');
    }

    public function create()
    {
        $products = Product::available()->get()->sortBy('name');
        return view('orders.create', compact('products'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $order = Order::findOrFail($request->id);
        $valid = true;

        switch ($request->status) {
            case 2:
                if ($order->status->isPending()) {
                    foreach ($order->orderItems as $key => $item) {
                        $product = $item->product;
                        $product->order_count += $item->quantity;
                        $product->timestamps = false;
                        $product->save();
                    }

                    try {
                        $order->user->sendOrderApprovedEmail($order);
                    } catch (Exception $e) {
                    }


                    $responseMessage = "Order is ready <b>To Ship</b>";
                } else {
                    $valid = false;
                    $responseMessage = "Order cannot change <b>To Ship</b>, please try to refresh the page";
                }
                break;
            case 3:
                if ($order->status->isToShip()) {
                    $responseMessage = "Order is ready <b>To Receive</b>";
                } else {
                    $valid = false;
                    $responseMessage = "Order cannot change <b>To Receive</b>, please try to refresh the page";
                }
                break;
            case 4:
                if ($order->status->isToReceive()) {
                    $responseMessage = "Order has been <b>Completed</b>";
                } else {
                    $valid = false;
                    $responseMessage = "Order could not be <b>Completed</b>, please try to refresh the page";
                }
                break;
            case 5:
                if ($order->status->isPending()) {
                    $responseMessage = "Order has been <b>Cancelled</b>";

                    foreach ($order->orderItems as $key => $item) {
                        $product = $item->product;
                        $product->quantity += $item->quantity;
                        $product->timestamps = false;
                        $product->save();
                    }
                } else {
                    $valid = false;
                    $responseMessage = "Order could not be <b>Cancelled</b>, please try to refresh the page";
                }
                break;
        }

        if (!$valid) {
            return response()->json(['error' => $responseMessage]);
        } else {
            $order->status_id = $request->status;
            $order->save();
        }

        $order->load('status');

        return response()->json(['success' => $responseMessage, 'data' => $order]);
    }

    public function addItem(Request $request)
    {
        $product = Product::findOrFail($request->product);

        if ($product->isOutOfStock()) {
            return response()->json([
                'error' => "Sorry, $product->name is out of stock. Please refresh the page to get the latest list."
            ]);
        }

        if ($request->qty > $product->quantity) {
            return response()->json([
                'error' => "Sorry, <b>$product->name</b> has only <b>$product->quantity</b> stocks left"
            ]);
        }

        $item = Cart::add($product, $request->qty);

        return response()->json([
            'success' => 'Product added to cart.',
            'data' => $item,
            'maxQuantity' => $item->model->quantity,
            'count' => Cart::content()->count(),
            'subtotal' => Cart::subtotal(),
            'vat' => Cart::tax()
        ]);
    }

    public function updateItem(Request $request)
    {
        $item = Cart::get($request->rowId);

        if ($request->qty > $item->model->quantity) {
            return response()->json([
                'error' => "Product <b><i>({$item->model->name})</i></b> has <b><i>{$item->model->quantity}</i></b> remaining stocks",
                'qty' => $item->qty
            ]);
        }

        Cart::update($request->rowId, $request->qty);
        return response()->json([
            'success' => 'Item successfully updated',
            'total' => number_format($item->price * $request->qty, 2),
            'subtotal' => Cart::subtotal(),
            'vat' => Cart::tax()
        ]);
    }

    public function deleteItem($rowId)
    {
        Cart::remove($rowId);
        return response()->json([
            'success' => 'Item successfully deleted',
            'count' => Cart::content()->count(),
            'subtotal' => Cart::subtotal(),
            'vat' => Cart::tax()
        ]);
    }

    public function checkout(Request $request)
    {
        $unavailableProducts = [];

        foreach (Cart::content() as $item) {
            if ($item->qty > $item->model->quantity) {
                $message = "<b>$item->name</b> has <b>{$item->model->quantity}</b> stocks left";
                $unavailableProducts[] = $message;
            }
        }

        if (count($unavailableProducts) > 0) {
            return redirect()->back()->with('customErrors', $unavailableProducts);
        }

        $order = Order::create([
            'user_id' => auth()->user()->id,
            'status_id' => 4,
            'name' => $request->name,
            'address' => $request->address
        ]);

        $orderItems = [];

        foreach (Cart::content() as $item) {
            $product = $item->model;
            $product->quantity = $product->quantity - $item->qty;
            $product->order_count = $product->order_count + $item->qty;
            $product->timestamps = false;
            $product->save();

            $orderItems[] = [
                'product_id' => $item->id,
                'quantity' => $item->qty
            ];
        }

        $order->orderItems()->createMany($orderItems);

        Cart::destroy();
        return redirect()->back()->with('success', 'Order successfully saved');
    }

    public function empty()
    {
        Cart::destroy();
        return redirect()->back()->with('success', 'Products have been removed from cart');
    }
}
