setRow = ({ id, name, description, created_at, updated_at })=> (`
    <tr class='row${id}'>
        <td class='btnActionContainer'>
            <button type="button" data-id="${id}" class="btn btn-success btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".editModal" title="edit">
                <i class="material-icons">edit</i>
            </button>
            &nbsp;
            <button type="button" data-id="${id}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                <i class="material-icons">delete</i>
            </button>
        </td>
        <td class="name-${id}">${name}</td>
        <td class="description-${id}">${ description ? description : ''}</td>
        <td data-order="${created_at}">${created_at}</td>
        <td class="updated_at-${id}" data-order="${updated_at}">${updated_at}</td>
    </tr>   
`);

setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

$(document).ready(() => {

    /* Variable declaration - initialization */
    const table = $(document).find('table');
    const addForm = $(document).find('.addForm');
    const addModal = $(document).find('.addModal');
    const editForm = $(document).find('.editForm');
    const editModal = $(document).find('.editModal');
    const deleteForm = $(document).find('.deleteForm');
    const deleteModal = $(document).find('.deleteModal');

    const dt = table.DataTable({
        order: [[3, "desc"]],
        "columnDefs": [
            { "orderable": false, "targets": 0 },
            { "searchable": false, "targets": 0 }
        ]
    });

    addForm.validate({
        rules: {
            name: {
                required: true,
                normalizer: value => $.trim(value)
            }, 
            description: {
                normalizer: value => $.trim(value)
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    addForm.submit(event => {
        event.preventDefault();

        if (addForm.valid()) {
            const name = $('.add-name').val();
            const description = $('.add-description').val();

            $.ajax({
                url: addForm.attr('action'),
                type: addForm.attr('method'),
                beforeSend: () => {
                    $('.btnSave').html('<i class="fa fa-spinner fa-spin"></i> Saving...');
                },
                data: { name, description },
                success: response => {
                    if (response.success) {
                        const row = setRow(response.data);

                        dt.row.add($(row)).draw(false);
                        addForm.trigger("reset");

                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnSave').html('SAVE')
            });
        }
    });

    editForm.validate({
        rules: {
            name: {
                required: true,
                normalizer: value => $.trim(value)
            }, 
            description: {
                normalizer: value => $.trim(value)
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    editModal.on('shown.bs.modal', e => {
        let id = e.relatedTarget.dataset.id;
        let name = $(`.name-${id}`).text();
        let description = $(`.description-${id}`).text();

        $('.edit-id').val(id);
        $('.edit-description').val(description ? description : '').focus();
        $('.edit-name').val(name).focus();
    });

    editForm.submit(event => {
        event.preventDefault();

        if (editForm.valid()) {
            const _method = editForm.attr('method');
            const id = $('.edit-id').val();
            const name = $('.edit-name').val();
            const description = $('.edit-description').val();

            $.ajax({
                url: `/categories/${id}`,
                type: _method,
                beforeSend: () => {
                    $('.btnUpdate').html('<i class="fa fa-spinner fa-spin"></i> Updating...');
                },
                data: { _method, id, name, description },
                success: response => {
                    if (response.success) {
                        const tempRow = dt.row(`.row${id}`).data();
                        tempRow[1] = response.data.name;
                        tempRow[2] = response.data.description;
                        tempRow[4].display = response.data.updated_at;
                        $(`.updated_at-${id}`).attr('data-order', response.data.updated_at);

                        editModal.modal('hide');
                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });

                        dt.row(`.row${id}`).data(tempRow).invalidate().draw(false);
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnUpdate').html('UPDATE')
            });
        }
    });

    deleteModal.on('show.bs.modal', e => {
        const id = e.relatedTarget.dataset.id;
        const name = $(`.name-${id}`).text();
        const description = $(`.description-${id}`).text();

        $('.delete-id').val(id);
        $('.delete-name').val(name);
        $('.delete-description').val(description ? description : '');
    });

    deleteForm.submit(event => {
        event.preventDefault();
        const _method = deleteForm.attr('method');
        const id = $('.delete-id').val();

        $.ajax({
            url: `/categories/${id}`,
            type: _method,
            beforeSend: () => {
                $('.btnDelete').html('<i class="fa fa-spinner fa-spin"></i> Deleting...');
            },
            data: { _method },
            success: response => {
                if (response.success) {
                    deleteModal.modal('hide');
                    iziToast.success({
                        title: 'Success',
                        message: response.success
                    });
                    dt.row(`.row${id}`).remove().draw(false);
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: setErrors(response.error),
                        timeout: false
                    });
                }
            },
            error: response => console.log(response),
            complete: () => $('.btnDelete').html('DELETE')
        });
    });

});
