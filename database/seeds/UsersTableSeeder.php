<?php

use App\User;
use App\Profile;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin Doe',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
            'role' => 'admin'
        ]);

        User::create([
            'name' => 'Staff Doe',
            'email' => 'staff@example.com',
            'password' => bcrypt('password'),
            'role' => 'staff'
        ]);

        $user = User::create([
            'name' => 'Customer Doe',
            'email' => 'customer@example.com',
            'password' => bcrypt('password'),
            'role' => 'customer'
        ]);

        Profile::create([
            'user_id' => $user->id,
            'mobile_no' => '9123456789',
            'building_street_info' => 'Blk 1 Lot 2, Straight Street, Village Subidivision',
            'barangay' => 'Balangay',
            'city' => 'Manila',
            'province' => 'Metro Manila',
            'region' => 'NCR',
            'postal_code' => '1234'
        ]);

        $supplier = User::create([
            'name' => 'Supplier Doe',
            'email' => 'supplier@example.com',
            'password' => bcrypt('password'),
            'role' => 'supplier'
        ]);

        Profile::create([
            'user_id' => $supplier->id,
            'mobile_no' => '9123456789',
            'building_street_info' => 'Blk 1 Lot 2, Straight Street, Village Subidivision',
            'barangay' => 'Balangay',
            'city' => 'Manila',
            'province' => 'Metro Manila',
            'region' => 'NCR',
            'postal_code' => '1234'
        ]);
    }
}
