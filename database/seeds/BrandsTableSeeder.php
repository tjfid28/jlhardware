<?php

use App\Brand;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        Brand::insert([
            [
                'name' => 'Panasonic',
                'slug' => 'panasonic',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Philips',
                'slug' => 'philips',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Moldex',
                'slug' => 'moldex',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'ABC',
                'slug' => 'abc',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Eagle',
                'slug' => 'eagle',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Nihonweld',
                'slug' => 'nihonweld',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Amerilock',
                'slug' => 'amerilock',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Redifix',
                'slug' => 'redifix',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Armak',
                'slug' => 'armak',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Boysen',
                'slug' => 'boysen',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Omega',
                'slug' => 'omega',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Bosny',
                'slug' => 'bosny',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Shurtape',
                'slug' => 'shurtape',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Jasco',
                'slug' => 'jasco',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Gleam',
                'slug' => 'gleam',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Sherman',
                'slug' => 'sherman',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Colorsteel',
                'slug' => 'colorsteel',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Cord',
                'slug' => 'cord',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Ergo',
                'slug' => 'ergo',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Stanley',
                'slug' => 'stanley',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => '3M',
                'slug' => '3m',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
