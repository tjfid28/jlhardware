@extends('layouts.backend')

@section('title', 'Products')

@section('styles')
    <!-- JQuery dataTables CSS -->
    <link href="{{ asset('css/backend/datatables.min.css') }}" rel="stylesheet">

    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">

    <!-- bootstrap-select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">

    <!-- Additional CSS -->
    <link href="{{ asset('css/backend/common.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        PRODUCTS
    @endcomponent

    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <div class="row clearfix">
                        <div class="col-md-6 col-xs-6">
                            <h2>List of Products</h2>
                        </div>

                        <div class="col-md-6 col-xs-6 text-right">
                            <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target=".addModal">
                                <i class="material-icons">add</i>
                                <span>Add new product</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="body">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">Actions</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Brand Id</th>
                                <th class="text-center">Brand</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Subcategory Id</th>
                                <th class="text-center">Subcategory</th>
                                <th class="text-center">Supplier Id</th>
                                <th class="text-center">Supplier</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">Updated At</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @forelse ($products as $product)
                                @php
                                    $bg = $product->isOutOfStock() ? 'bg-grey' : ($product->isCritical() ? 'bg-red' : '');
                                @endphp
                                <tr class="row{{ $product->id }} {{ $bg }}">
                                    <td class="btnActionContainer">
                                        <button type="button" data-id="{{ $product->id }}" class="btn btn-success btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".editModal" title="edit">
                                            <i class="material-icons">edit</i>
                                        </button>
                                        &nbsp;
                                        <button type="button" data-id="{{ $product->id }}" class="btn btn-danger btn-circle btn-action waves-effect waves-circle waves-float" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                    <td class="image-{{ $product->id }}">
                                        <a href="{{ $product->getImageUrl() }}" target="_blank">
                                            <img src="{{ $product->getImageUrl() }}" alt="{{ $product->getImageUrl() }}" width="48" height="24">
                                        </a>
                                    </td>
                                    <td class="name-{{ $product->id }}">{{ $product->name }}</td>
                                    <td class="brand_id-{{ $product->id }}">{{ $product->brand_id }}</td>
                                    <td class="brand_name-{{ $product->id }}">{{ $product->brand->name }}</td>
                                    <td class="category-{{ $product->id }}">{{ $product->category->name }}</td>
                                    <td class="subcategory_id-{{ $product->id }}">{{ $product->subcategory_id }}</td>
                                    <td class="subcategory_name-{{ $product->id }}">{{ $product->subcategory->name }}</td>
                                    <td class="supplier_id-{{ $product->id }}">{{ $product->supplier_id }}</td>
                                    <td class="supplier_name-{{ $product->id }}">{{ $product->supplier->name }}</td>
                                    <td class="price-{{ $product->id }}" data-price="{{ $product->price }}">{{ number_format($product->price, 2) }}</td>
                                    <td class="quantity-{{ $product->id }}">{{ $product->quantity }}</td>
                                    <td class="description-{{ $product->id }}">{{ $product->description }}</td>
                                    <td data-order="{{ $product->created_at }}">{{ $product->created_at }}</td>
                                    <td class="updated_at-{{ $product->id }}" data-order="{{ $product->updated_at }}">{{ $product->updated_at }}</td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center">Actions</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Brand Id</th>
                                <th class="text-center">Brand</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Subcategory Id</th>
                                <th class="text-center">Subcategory</th>
                                <th class="text-center">Supplier Id</th>
                                <th class="text-center">Supplier</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">Updated At</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="critical_level" value="{{ config('env.critical_level') }}">

    <div class="modal fade addModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Add new product</h4>
                </div>
                <form class="addForm" action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Image*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" class="form-control add-image" name="image" accept="image/jpg,image/jpeg,image/png">
                                    </div>
                                    <div class="help-info">Image: jpg|jpeg|png</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control add-name" name="name">
                                <label class="form-label">Name*</label>
                            </div>
                        </div>

                        <label for="brand_id">Brand*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control add-brand selectpicker show-tick" name="brand_id" data-live-search="true">
                                    <option value="">-- Please select a brand --</option>
                                    @forelse ($brands as $brand)
                                        <option value="{{ $brand->id }}" data-tokens="{{ $brand->name }}">{{ $brand->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <label for="subcategory_id">Subcategory*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control add-subcategory selectpicker show-tick" name="subcategory_id" data-live-search="true">
                                    <option value="">-- Please select a subcategory --</option>
                                    @forelse ($categories as $category)
                                        @if (count($category->subcategories) > 0)
                                            <optgroup label="{{ $category->name }}">
                                                @foreach ($category->subcategories as $subcategory)
                                                    <option value="{{ $subcategory->id }}" data-tokens="{{ $subcategory->name }}">{{ $subcategory->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endif
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <label for="brand_id">Supplier*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control add-supplier selectpicker show-tick" name="supplier_id" data-live-search="true">
                                    <option value="">-- Please select a supplier --</option>
                                    @forelse ($suppliers as $supplier)
                                        <option value="{{ $supplier->id }}" data-tokens="{{ $supplier->name }}">{{ $supplier->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control add-price" name="price">
                                <label class="form-label">Price*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control add-quantity" name="quantity">
                                <label class="form-label">Quantity*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize add-description" name="description"></textarea>
                                <label class="form-label">Description</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnSave waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Edit product</h4>
                </div>
                <form class="editForm" method="PATCH">
                    <div class="modal-body">
                        <input class="edit-id" type="hidden" name="id">

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Image</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" class="form-control edit-image" name="image" accept="image/jpg,image/jpeg,image/png">
                                    </div>
                                    <div class="help-info">Image: jpg|jpeg|png</div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control edit-name" name="name">
                                <label class="form-label">Name*</label>
                            </div>
                        </div>

                        <label for="brand_id">Brand*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control edit-brand selectpicker show-tick" name="brand_id" data-live-search="true">
                                    <option value="">-- Please select a brand --</option>
                                    @forelse ($brands as $brand)
                                        <option value="{{ $brand->id }}" data-tokens="{{ $brand->name }}">{{ $brand->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <label for="subcategory_id">Subcategory*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control edit-subcategory selectpicker show-tick" name="subcategory_id" data-live-search="true">
                                    <option value="">-- Please select a subcategory --</option>
                                    @forelse ($categories as $category)
                                        @if (count($category->subcategories) > 0)
                                            <optgroup label="{{ $category->name }}">
                                                @foreach ($category->subcategories as $subcategory)
                                                    <option value="{{ $subcategory->id }}" data-tokens="{{ $subcategory->name }}">{{ $subcategory->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endif
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <label for="supplier_id">Supplier*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control edit-supplier selectpicker show-tick" name="supplier_id" data-live-search="true">
                                    <option value="">-- Please select a supplier --</option>
                                    @forelse ($suppliers as $supplier)
                                        <option value="{{ $supplier->id }}" data-tokens="{{ $supplier->name }}">{{ $supplier->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control edit-price" name="price">
                                <label class="form-label">Price*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control edit-quantity" name="quantity">
                                <label class="form-label">Quantity*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize edit-description" name="description"></textarea>
                                <label class="form-label">Description</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnUpdate waves-effect">UPDATE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Delete product</h4>
                </div>
                <form class="deleteForm form-horizontal" method="DELETE">
                    <div class="modal-body">
                        <h5>Are you sure you want to delete this data?</h5>
                        <input class="delete-id" type="hidden" name="id">

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Image</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <img class="img-responsive thumbnail delete-image">
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Name</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-name" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Brand</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-brand" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Category</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-category" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Subcategory</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-subcategory" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Supplier</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-supplier" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Price</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-price" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Quantity</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control delete-quantity" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Description</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <textarea rows="4" class="form-control no-resize delete-description" disabled></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link btnDelete waves-effect">DELETE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- JQuery dataTables JS -->
    <script type="text/javascript" src="{{ asset('js/backend/datatables.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- bootstrap-select Js -->
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/products.js') }}"></script>
@endsection
