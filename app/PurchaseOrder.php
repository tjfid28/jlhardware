<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $fillable = ['supplier_id', 'status_id', 'description'];

    public function supplier()
    {
        return $this->belongsTo('App\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function getPurchaseOrderNo()
    {
        return "PO#-$this->id";
    }

    public function isNotDeletable()
    {
        return !($this->status_id == 5 || $this->status_id == 6);
    }

    public function scopeBelongsToSupplier($query)
    {
        return $query->where('supplier_id', auth()->user()->id);
    }
}
