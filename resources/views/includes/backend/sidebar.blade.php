<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ asset('images/user.png') }}" width="48" height="48" alt="User" />
        </div>

        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:void(0);"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i> Sign Out
                        </a>
                    </li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                    @csrf
                </form>
            </div>
        </div>
    </div>
    <!-- #User Info -->

    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            @if (Auth::user()->isSupplier())
                <li class="{{ Request::is('purchase-orders*') ? 'active' : null }}">
                    <a href="{{ route('purchase-orders.index') }}">
                        <i class="material-icons">list</i>
                        <span>Purchase Orders</span>
                    </a>
                </li>
            @else
                <li class="{{ Request::is('dashboard') ? 'active' : null }}">
                    <a href="{{ route('dashboard') }}">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ Request::is('products') || Request::is('inventory/fast-moving-items') || Request::is('inventory/slow-moving-items') || Request::is('inventory/critical-level') ? 'active' : null }}">
                    <a href="javascript:void(0)" class="menu-toggle">
                        <i class="material-icons">store</i>
                        <span>Inventory</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="{{ Request::is('products') ? 'active' : null }}">
                            <a href="{{ route('products.index') }}">Products</a>
                        </li>
                        <li class="{{ Request::is('inventory/fast-moving-items') ? 'active' : null }}">
                            <a href="{{ route('inventory.fastMovingItems') }}">Fast Moving Items</a>
                        </li>
                        <li class="{{ Request::is('inventory/slow-moving-items') ? 'active' : null }}">
                            <a href="{{ route('inventory.slowMovingItems') }}">Slow Moving Items</a>
                        </li>
                        <li class="{{ Request::is('inventory/critical-level') ? 'active' : null }}">
                            <a href="{{ route('inventory.criticalLevel') }}">Critical Level</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::is('orders') || Request::is('orders/create') ? 'active' : null }}">
                    <a href="javascript:void(0)" class="menu-toggle" href="{{ route('orders.index') }}">
                        <i class="material-icons">shopping_cart</i>
                        <span>Orders</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="{{ Request::is('orders') ? 'active' : null }}">
                            <a href="{{ route('orders.index') }}">View</a>
                        </li>
                        <li class="{{ Request::is('orders/create') ? 'active' : null }}">
                            <a href="{{ route('orders.create') }}">Create</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::is('purchase-orders*') ? 'active' : null }}">
                    <a href="{{ route('purchase-orders.index') }}">
                        <i class="material-icons">list</i>
                        <span>Purchase Orders</span>
                    </a>
                </li>
                @if (Auth::user()->isAdmin())
                    <li class="{{ Request::is('users') ? 'active' : null }}">
                        <a href="{{ route('users.index') }}">
                            <i class="material-icons">people</i>
                            <span>Users</span>
                        </a>
                    </li>

                    <li class="{{ Request::is('suppliers') ? 'active' : null }}">
                        <a href="{{ route('suppliers.index') }}">
                            <i class="material-icons">supervised_user_circle</i>
                            <span>Suppliers</span>
                        </a>
                    </li>
                @endif
                <li class="{{ Request::is('reports') ? 'active' : null }}">
                    <a href="{{ route('reports.index') }}">
                        <i class="material-icons">table_chart</i>
                        <span>Reports</span>
                    </a>
                </li>
                <li class="{{ Request::is('brands') || Request::is('categories') || Request::is('subcategories') ? 'active' : null }}">
                    <a href="javascript:void(0)" class="menu-toggle">
                        <i class="material-icons">category</i>
                        <span>Product Settings</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="{{ Request::is('brands') ? 'active' : null }}">
                            <a href="{{ route('brands.index') }}">Brands</a>
                        </li>
                        <li class="{{ Request::is('categories') ? 'active' : null }}">
                            <a href="{{ route('categories.index') }}">Categories</a>
                        </li>
                        <li class="{{ Request::is('subcategories') ? 'active' : null }}">
                            <a href="{{ route('subcategories.index') }}">Subcategories</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::is('slides') || Request::is('about/edit') || Request::is('contact/edit') || Request::is('faqs') || Request::is('terms')  ? 'active' : null }}">
                    <a href="javascript:void(0)" class="menu-toggle">
                        <i class="material-icons">settings</i>
                        <span>Page Settings</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="{{ Request::is('slides') ? 'active' : null }}">
                            <a href="{{ route('slides.index') }}">Slides</a>
                        </li>
                        <li class="{{ Request::is('about/edit') ? 'active' : null }}">
                            <a href="{{ route('about.edit') }}">About</a>
                        </li>
                        <li class="{{ Request::is('contact/edit') ? 'active' : null }}">
                            <a href="{{ route('contacts.edit') }}">Contact</a>
                        </li>
                        <li class="{{ Request::is('faqs') ? 'active' : null }}">
                            <a href="{{ route('faqs.index') }}">Frequently Asked Questions</a>
                        </li>
                        <li class="{{ Request::is('terms') ? 'active' : null }}">
                            <a href="{{ route('terms.index') }}">Terms and Conditions</a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
    <!-- #Menu -->

    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; {{ date('Y') }} <a href="javascript:void(0);">{{ config('app.name') }}</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.0
        </div>
    </div>
    <!-- #Footer -->
</aside>
