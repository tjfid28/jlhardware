<?php

namespace App\Http\Controllers;

use App\User;
use App\PurchaseOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PurchaseOrdersController extends Controller
{
    public function index()
    {
        if (auth()->user()->isAdmin()) {
            $purchaseOrders = PurchaseOrder::latest()->paginate(5);
        } else {
            $purchaseOrders = PurchaseOrder::belongsToSupplier()->latest()->paginate(5);
        }

        return view('purchase-orders.index', compact('purchaseOrders'));
    }

    public function create()
    {
        $suppliers = User::where('role', 'supplier')->get()->sortBy('name');
        return view('purchase-orders.create', compact('suppliers'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'supplier_id' => 'required',
            'description' => 'required'
        ]);

        PurchaseOrder::create([
            'supplier_id' => $request->supplier_id,
            'status_id' => 1,
            'description' => $request->description
        ]);

        return redirect()->back()->with('success', 'Purchase order successfully saved');
    }

    public function edit(PurchaseOrder $purchaseOrder)
    {
        if (!$purchaseOrder->status->isPending()) {
            return redirect()->route('purchase-orders.index')->with('warning', 'Cannot edit unpending Purchase Order');
        }

        $suppliers = User::where('role', 'supplier')->get()->sortBy('name');
        return view('purchase-orders.edit', compact('purchaseOrder', 'suppliers'));
    }

    public function update(Request $request, PurchaseOrder $purchaseOrder)
    {
        $request->validate([
            'supplier_id' => 'required',
            'description' => 'required'
        ]);

        if (!$purchaseOrder->status->isPending()) {
            return redirect()->route('purchase-orders.index')->with('error', 'Cannot update unpending Purchase Order');
        }

        $purchaseOrder->supplier_id = $request->supplier_id;
        $purchaseOrder->description = $request->description;
        $purchaseOrder->save();

        return redirect()->back()->with('success', 'Purchase order successfully updated');
    }

    public function cancel(PurchaseOrder $purchaseOrder)
    {
        if (!$purchaseOrder->status->isPending()) {
            return redirect()->back()->with('error', 'Cannot cancel unpending Purchase Order');
        }

        $purchaseOrder->status_id = 5;
        $purchaseOrder->save();

        return redirect()->back()->with('success', 'Purchase order has been cancelled');
    }

    public function receive(PurchaseOrder $purchaseOrder)
    {
        if (!$purchaseOrder->status->isCompleted()) {
            return redirect()->back()->with('error', 'Cannot receive uncompleted Purchase Order');
        }

        $purchaseOrder->status_id = 6;
        $purchaseOrder->save();

        return redirect()->back()->with('success', 'Purchase order has been received');
    }

    public function destroy(PurchaseOrder $purchaseOrder)
    {
        if ($purchaseOrder->isNotDeletable()) {
            return redirect()->back()->with('error', 'Cannot delete unreceived and uncancelled Purchase Order');
        }

        $purchaseOrder->delete();

        return redirect()->back()->with('success', 'Purchase order has been deleted');
    }

    public function complete(PurchaseOrder $purchaseOrder)
    {
        if (!$purchaseOrder->status->isPending()) {
            return redirect()->back()->with('error', 'Cannot complete unpending Purchase Order');
        }

        $purchaseOrder->status_id = 4;
        $purchaseOrder->save();

        return redirect()->back()->with('success', 'Purchase order has been completed');
    }
}
