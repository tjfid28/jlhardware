@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <section class="bg-texture">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @foreach ($slides as $key => $slide)
                    <li data-target="#myCarousel" data-slide-to="{{ $key }}" class="{{ $key === 0 ? 'active' : null }}"></li>
                @endforeach
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach ($slides as $key => $slide)
                    <div class="item {{ $key === 0 ? 'active' : null }}">
                        <img src="{{ $slide->getImageUrl() }}" alt="image">
                        <div class="carousel-caption"></div>
                    </div>
                @endforeach
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="container">
            <div class="home-header">Latest Products</div>

            <div class="container box-products">
                <div class="row product-container">
                    @forelse ($products as $key => $product)
                        @if ($key > 11)
                            @break
                        @endif

                        <div class="col-md-3" style="margin-bottom: 30px;">
                            <div class='product-image-wrapper'>
                                <div class="main-prod">
                                    <div class="productinfo text-center">
                                        <p>
                                            <a href="{{ route('products.show', $product->slug) }}" rel="bookmark" title="{{ $product->name }}">
                                                <img src="{{ $product->getImageUrl() }}" alt="{{ $product->name }}" title="{{ $product->name }}" width="150" height="150" />
                                            </a>
                                        </p>

                                        <p>
                                            <a href="{{ route('products.show', $product->slug) }}" rel="bookmark" title="{{ $product->name }}">
                                                {{ $product->name }}
                                            </a>
                                        </p>

                                        <p>Price: {{ number_format($product->price, 2) }}</p>

                                        <a href="{{ route('products.show', $product->slug) }}" class="btn btn-default add-to-cart">
                                            <i class="fa fa-shopping-cart"></i> View Details
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <h2 class="text-center">No records found</h2>
                    @endforelse

                    @if (count($products) > 11)
                        <div class="col-md-12 text-center">
                            <h3><a href="{{ route('categories.show', 'all') }}">View More</a></h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
