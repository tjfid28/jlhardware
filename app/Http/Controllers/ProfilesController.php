<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('customer');
    }

    public function index()
    {
        return view('frontend.profile');
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_no' => 'required|digits:10',
            'telephone_no' => 'nullable|digits:7',
            'building_street_info' => 'required|string|max:191',
            'barangay' => 'required|string|max:191',
            'city' => 'required|string|max:191',
            'province' => 'required|string|max:191',
            'region' => 'required|string|max:191',
            'postal_code' => 'required|digits:4',
        ]);

        /*if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors()->all());
        }*/

        $profile = auth()->user()->profile;
        $profile->mobile_no = $request->mobile_no;
        $profile->telephone_no = $request->telephone_no;
        $profile->building_street_info = $request->building_street_info;
        $profile->barangay = $request->barangay;
        $profile->city = $request->city;
        $profile->province = $request->province;
        $profile->region = $request->region;
        $profile->postal_code = $request->postal_code;
        $profile->save();

        return redirect()->back()->with('success', 'Profile successfully updated');
    }
}
