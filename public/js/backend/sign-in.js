$(function () {
    $('form').validate({
        rules: {
            email: {
                required: true,
                email: true,
                normalizer: value => $.trim(value)
            }, 
            password: {
                required: true,
                normalizer: value => $.trim(value)
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        }
    });
});
