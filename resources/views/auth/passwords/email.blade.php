@extends('layouts.auth')

@section('title', 'Forgot Password')

@section('content')
<form method="POST" action="{{ route('password.email') }}">
    @csrf

    <div class="msg">
        Enter your email address that you used to register. We'll send you an email with a
        link to reset your password.
    </div>

    @include('includes.message')

    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">email</i>
        </span>
        <div class="form-line">
            <input type="email" class="form-control" name="email" placeholder="Email" autofocus>
        </div>
    </div>

    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">RESET MY PASSWORD</button>

    <div class="row m-t-20 m-b--5 align-center">
        <a href="{{ route('login') }}">Sign In!</a>
    </div>
</form>
@endsection

@section('scripts')
    <script src="{{ asset('js/backend/forgot-password.js') }}"></script>
@endsection
