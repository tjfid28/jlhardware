@extends('layouts.backend')

@section('title', 'Dashboard')

@section('styles')
    <style type="text/css">
        a.col-md-3:hover {
            text-decoration: none;
        }
        a .hover-expand-effect, a .info-box-2 {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    @component('components.backend.block-header')
        DASHBOARD
    @endcomponent

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="body">
                    <h4>Orders</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="info-box-2 bg-grey hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="content">
                                    <div class="text">PENDING</div>
                                    <div class="number">{{ $orderCount['pending'] }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info-box-2 bg-blue hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="content">
                                    <div class="text">TO SHIP</div>
                                    <div class="number">{{ $orderCount['to-ship'] }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="info-box-2 bg-orange hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="content">
                                    <div class="text">TO RECEIVE</div>
                                    <div class="number">{{ $orderCount['to-receive'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="info-box-2 bg-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="content">
                                    <div class="text">COMPLETED</div>
                                    <div class="number">{{ $orderCount['completed'] }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-md-offset-1">
                            <div class="info-box-2 bg-red hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="content">
                                    <div class="text">CANCELLED</div>
                                    <div class="number">{{ $orderCount['cancelled'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="body">
                    <h4>Inventory</h4>
                    <div class="row">
                        <a class="col-md-3" href="{{ route('products.index') }}">
                            <div class="info-box-2 bg-deep-purple hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">store_mall_directory</i>
                                </div>
                                <div class="content">
                                    <div class="text">ALL</div>
                                    <div class="number">{{ $inventoryCount['all'] }}</div>
                                </div>
                            </div>
                        </a>
                        <a class="col-md-3" href="{{ route('inventory.fastMovingItems') }}">
                            <div class="info-box-2 bg-cyan hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">store_mall_directory</i>
                                </div>
                                <div class="content">
                                    <div class="text">FAST MOVING</div>
                                    <div class="number">{{ $inventoryCount['fast-moving'] }}</div>
                                </div>
                            </div>
                        </a>
                        <a class="col-md-3" href="{{ route('inventory.slowMovingItems') }}">
                            <div class="info-box-2 bg-lime hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">store_mall_directory</i>
                                </div>
                                <div class="content">
                                    <div class="text">SLOW MOVING</div>
                                    <div class="number">{{ $inventoryCount['slow-moving'] }}</div>
                                </div>
                            </div>
                        </a>
                        <a class="col-md-3" href="{{ route('inventory.criticalLevel') }}">
                            <div class="info-box-2 bg-deep-orange hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">store_mall_directory</i>
                                </div>
                                <div class="content">
                                    <div class="text">CRITICAL LEVEL</div>
                                    <div class="number">{{ $inventoryCount['critical-level'] }}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
