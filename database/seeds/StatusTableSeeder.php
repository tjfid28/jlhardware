<?php

use App\Status;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        Status::insert([
            [
                'name' => 'Pending',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'To Ship',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'To Receive',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Completed',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Cancelled',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Received',
                'description' => $faker->text(191),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
