@extends('layouts.app')

@section('title', 'Cart')

@section('styles')
    <!-- jQuery Spinner CSS -->
    <link href="{{ asset('css/frontend/bootstrap-spinner.min.css') }}" rel="stylesheet">

    <!-- iziToast CSS -->
    <link href="{{ asset('css/iziToast.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!--form-->
    <section id="form">
        <div class="container">
            @include('includes.message')

            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        <b>Note: </b> Minimum amount of <b>{{ number_format(config('env.minimum_order_amount'), 2) }}</b> to place order.
                    </div>
                </div>
                <div class="col-sm-12">
                    <table class="table table-bordered table-responsive">
                        <thead class="bg-primary" style="font-size: 1.3em;">
                            <th class="text-center">Item</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">Total</th>
                            <th></th>
                        </thead>
                        <tbody style="font-size:1.2em;">
                            @forelse (Cart::content() as $item)
                                <tr data-rowid={{ $item->rowId }} data-id="{{ $item->id }}">
                                    <td class="text-center"><strong>{{ $item->name }}</strong></td>
                                    <td class="text-center">{{ number_format($item->price, 2) }}</td>
                                    <td class="text-center">
                                        <div data-trigger="spinner" class="form-inline spinner">
                                            <button type="button" class="btn btn-default" data-spin="down">-</button>
                                            <input name="qty" type="text" class="form-control qty" value="{{ $item->qty }}" data-rule="quantity" data-max="{{ $item->model->quantity }}">
                                            <button type="button" class="btn btn-default" data-spin="up">+</button>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <font class="itotal">
                                            {{ number_format($item->price * $item->qty, 2) }}
                                        </font>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" data-id="{{ $item->rowId }}" data-name="{{ $item->name }}" class="btn btn-danger btn-circle" data-tooltip="tooltip" data-toggle="modal" data-placement="top" data-target=".deleteModal" title="delete">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center alert alert-danger"><strong>*** Your Cart is Empty ***</strong></td>
                                </tr>
                            @endforelse

                            @if (Cart::content()->count())
                                <tr>
                                    <td colspan="3" class="text-right"><strong>Sub Total</strong></td>
                                    <td colspan="2" class="text-primary subtotal">{{ Cart::subtotal() }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-right"><strong>VAT ({{ config('cart.tax') }}%)</strong></td>
                                    <td colspan="2" class="text-danger vat">{{ Cart::tax() }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-right"><strong>Amount Due</strong></td>
                                    <td colspan="2" class="text-danger amount-due"><strong>{{ Cart::subtotal() }}</strong></td>
                                </tr>
                            @endif
                        </tbody>
                    </table>

                    <input type="hidden" class="tax" value="{{ number_format(config('cart.tax') / 100, 2) }}">
                    <input type="hidden" class="minimum-order-amount" value="{{ config('env.minimum_order_amount') }}">

                    @if (Cart::count())
                        <div class="pull-right">
                            <a class="btn btn-primary btn-lg" style="margin-top: 0" href="{{ route('categories.show', 'all') }}">Continue Shopping</a>
                            <button class="btn btn-danger btn-lg" data-toggle="modal" data-target=".emptyModal">Empty Cart</button>
                            <button class="btn btn-success btn-lg btn-check-out {{ Cart::subtotal(2, '.', '') < config('env.minimum_order_amount') ? 'hidden' : null }}" data-toggle="modal" data-target=".checkoutModal">Check Out</button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!--form-->

    <!-- Remove product modal -->
    <div class="modal fade deleteModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Remove product from cart</h4>
                </div>
                <form class="deleteForm" action="" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <h5>Are you sure you want to remove this product?</h5>
                        <input class="delete-rowId" type="hidden" name="rowId">

                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control delete-name" type="text" value="{{ Auth::user()->name }}" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">REMOVE</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Remove item modal -->

    <!-- Empty card modal -->
    <div class="modal fade emptyModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('cart.empty') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <h4>Are you sure you want to remove all products from cart?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Empty card modal -->

    <!-- Checkout modal -->
    <div class="modal fade checkoutModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">
                        <i class="fa fa-shopping-cart text-success fa-lg"></i> Check Out
                        <small class='text-primary'> Billing Information</small>
                    </h4>
                </div>
                <form action="{{ route('orders.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" type="text" value="{{ Auth::user()->name }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Mobile No.</label>
                            <input class="form-control" type="text" value="{{ Auth::user()->profile->getFormattedMobileNo() }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Telephone No.</label>
                            <input class="form-control" type="text" value="{{ Auth::user()->profile->telephone_no ? Auth::user()->profile->getFormattedTelephoneNo() : '' }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" type="text" value="{{ Auth::user()->email }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" readonly>{{ Auth::user()->profile->getAddress() }}</textarea>
                        </div>
                        <div class="alert alert-info">
                            Mode of Payment: <strong>Pay on Delivery</strong>
                        </div>
                        <div class="alert alert-warning">
                            *** Please wait for our call/text or email for confirmation. Thank You! ***
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Checkout modal -->
@endsection

@section('scripts')
    <!-- Spinner Plugin Js -->
    <script src="{{ asset('js/frontend/jquery.spinner.min.js') }}"></script>

    <!-- iziToast Js -->
    <script src="{{ asset('js/iziToast.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/frontend/cart.js') }}"></script>
@endsection
