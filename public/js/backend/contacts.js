setErrors = errors => {
    let list = '<ul>';
    $.each(errors, (key, value) => {
        list += `<li>${value}</li>`;
    });
    list += '</ul>'

    return list;
}

$(document).ready(() => {

    const editForm = $(document).find('.editForm');

    editForm.validate({
        rules: {
            address: {
                required: true,
                maxlength: 191,
                normalizer: value => $.trim(value)
            },
            mobile_no: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10,
                normalizer: value => $.trim(value),
            },
            telephone_no: {
                number: true,
                minlength: 7,
                maxlength: 7,
                normalizer: value => $.trim(value),
            },
            email: {
                required: true,
                email: true,
                normalizer: value => $.trim(value)
            },
            business_hours: {
                required: true,
                maxlength: 191,
                normalizer: value => $.trim(value)
            },
        },
        messages: {
            mobile_no : {
                minlength: "Please enter a valid mobile no.",
                maxlength: "Please enter a valid mobile no.",
            },
            telephone_no : {
                minlength: "Please enter a valid telephone no.",
                maxlength: "Please enter a valid telephone no.",
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    editForm.submit(event => {
        event.preventDefault();

        if (editForm.valid()) {
            const _method = editForm.attr('method');
            const address = $('.edit-address').val();
            const mobile_no = $('.edit-mobile_no').val();
            const telephone_no = $('.edit-telephone_no').val();
            const email = $('.edit-email').val();
            const business_hours = $('.edit-business_hours').val();

            $.ajax({
                url: editForm.attr('action'),
                type: _method,
                beforeSend: () => {
                    $('.btnUpdate').html('<i class="fa fa-spinner fa-spin"></i> Updating...');
                },
                data: { _method, address, mobile_no, telephone_no, email, business_hours },
                success: response => {
                    if (response.success) {
                        iziToast.success({
                            title: 'Success',
                            message: response.success
                        });
                    } else {
                        iziToast.error({
                            title: 'Error',
                            message: setErrors(response.errors),
                            timeout: false
                        });
                    }
                },
                error: response => console.log(response),
                complete: () => $('.btnUpdate').html('UPDATE')
            });
        }
    });

});
