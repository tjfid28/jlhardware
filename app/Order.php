<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'status_id', 'name', 'address'];

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function getOrderNumber()
    {
        return "#ORD-$this->id";
    }

    public function getTotalAmount()
    {
        $totalAmount = 0;

        foreach ($this->orderItems as $key => $item) {
            $totalAmount += ($item->product->price * $item->quantity);
        }

        return $totalAmount;
    }
}
