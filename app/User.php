<?php

namespace App;

use App\Order;
use App\Notifications\OrderPlaced;
use App\Notifications\OrderApproved;
use App\Notifications\EmailVerification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function purchaseOrders()
    {
        return $this->hasMany('App\PurchaseOrder');
    }

    public function sendVerificationEmail()
    {
        $this->notify(new EmailVerification($this));
    }

    public function sendOrderApprovedEmail(Order $order)
    {
        $this->notify(new OrderApproved($order));
    }

    public function sendOrderPlacedEmail(Order $order)
    {
        $this->notify(new OrderPlaced($order));
    }

    public function isVerified()
    {
        return is_null($this->token);
    }

    public function isAdmin()
    {
        return strcmp($this->role, "admin") === 0;
    }

    public function isStaff()
    {
        return strcmp($this->role, "staff") === 0;
    }

    public function isCustomer()
    {
        return strcmp($this->role, "customer") === 0;
    }

    public function isSupplier()
    {
        return strcmp($this->role, "supplier") === 0;
    }
}
