<?php

use App\Product;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $now = Carbon::now()->toDateTimeString();

        Product::insert([
            [
                'brand_id' => 1,
                'category_id' => 1,
                'subcategory_id' => 1,
                'supplier_id' => 4,
                'name' => 'Battery Panasonic AA',
                'slug' => str_slug('Battery Panasonic AA'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'Battery Panasonic AA.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 4,
                'category_id' => 2
,                'subcategory_id' => 3,
                'supplier_id' => 4,
                'name' => 'ABC Tile Adhesive TBA',
                'slug' => str_slug('ABC Tile Adhesive TBA'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'ABC-TILE-ADHESIVE-TBA.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 9,
                'category_id' => 2,
                'subcategory_id' => 4,
                'supplier_id' => 4,
                'name' => 'ARMAK Rubber Tape',
                'slug' => str_slug('ARMAK Rubber Tape'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'ARMAK Rubber Tape.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 10,
                'category_id' => 4,
                'subcategory_id' => 7,
                'supplier_id' => 4,
                'name' => 'Boysen',
                'slug' => str_slug('Boysen'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'Boysen.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 14,
                'category_id' => 4,
                'subcategory_id' => 8,
                'supplier_id' => 4,
                'name' => 'Paint Remover JASCO',
                'slug' => str_slug('Paint Remover JASCO'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'Paint Remover JASCO.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 15,
                'category_id' => 5,
                'subcategory_id' => 9,
                'supplier_id' => 4,
                'name' => 'Liquid Sosa Kleen',
                'slug' => str_slug('Liquid Sosa Kleen'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'Liquid Sosa Kleen.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 17,
                'category_id' => 6,
                'subcategory_id' => 11,
                'supplier_id' => 4,
                'name' => 'Molave',
                'slug' => str_slug('Molave'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'Molave.png',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 18,
                'category_id' => 6,
                'subcategory_id' => 12,
                'supplier_id' => 4,
                'name' => 'Cord Water Stop',
                'slug' => str_slug('Cord Water Stop'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'Cord Water Stop.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 19,
                'category_id' => 7,
                'subcategory_id' => 13,
                'supplier_id' => 4,
                'name' => 'Shovel ERGO Square',
                'slug' => str_slug('Shovel ERGO Square'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => 'Shovel ERGO Square.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'brand_id' => 21,
                'category_id' => 7,
                'subcategory_id' => 14,
                'supplier_id' => 4,
                'name' => '3M Cutoff Wheel for STEEL',
                'slug' => str_slug('3M Cutoff Wheel for STEEL'),
                'price' => $faker->randomFloat(2, 5, 1000),
                'quantity' => $faker->numberBetween(0, 100),
                'description' => $faker->text(191),
                'image' => '3M Cutoff Wheel for STEEL.jpg',
                'order_count' => $faker->numberBetween(0, 20),
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
