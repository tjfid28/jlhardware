$(document).ready(() => {
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

    $('.cancelModal').on('show.bs.modal', function (e) {
        const id = e.relatedTarget.dataset.id;
        $('.cancel-id').val(id);
    });

    $('.cancel-item-modal').on('show.bs.modal', function (e) {
        const id = e.relatedTarget.dataset.id;
        const name = e.relatedTarget.dataset.name;

        $('.cancel-item-modal-header').html(`Are you sure you want to cancel item: <b><i>${name}</i></b>?`);
        $('.cancel-item-id').val(id);
    });
});
