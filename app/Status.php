<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "status";

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function purchaseOrders()
    {
        return $this->hasMany('App\PurchaseOrder');
    }

    public function getLabel()
    {
        $label = null;

        switch ($this->id) {
            case 1:
                $label = 'label-default';
                break;
            case 2:
                $label = 'label-primary';
                break;
            case 3:
                $label = 'label-warning';
                break;
            case 4:
                $label = 'label-success';
                break;
            case 5:
                $label = 'label-danger';
                break;
        }

        return $label;
    }

    public function isPending()
    {
        return $this->id === 1;
    }

    public function isToShip()
    {
        return $this->id === 2;
    }

    public function isToReceive()
    {
        return $this->id === 3;
    }

    public function isCompleted()
    {
        return $this->id === 4;
    }

    public function isCancelled()
    {
        return $this->id === 5;
    }

    public function isReceived()
    {
        return $this->id === 6;
    }

    public function getSafeName()
    {
        return str_slug($this->name);
    }
}
