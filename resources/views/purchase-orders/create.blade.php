@extends('layouts.backend')

@section('title', 'Create Purchase Orders')

@section('styles')
    <!-- bootstrap-select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet">

    <!-- froala editor CSS -->
    <link href="{{ asset('css/backend/froala_editor.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('components.backend.block-header')
        CREATE PURCHASE ORDER
    @endcomponent

    <div class="row p-b-10">
        <div class="col-md-12">
            <a href="{{ route('purchase-orders.index') }}" class="btn btn-default waves-effect">
                <i class="material-icons">arrow_back_ios</i>
                <span>Back to list</span>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                   <h2>Add new purchase order</h2>
                </div>
                <div class="body">
                    @include('includes.message')

                    <form action="{{ route('purchase-orders.store') }}" method="POST" class="form">
                        @csrf

                        <label for="brand_id">Supplier*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <select class="form-control selectpicker show-tick" name="supplier_id" data-live-search="true">
                                    <option value="">-- Please select a supplier --</option>
                                    @forelse ($suppliers as $supplier)
                                        <option value="{{ $supplier->id }}" data-tokens="{{ $supplier->name }}" {{ old('supplier_id') == $supplier->id ? 'selected' : '' }}>{{ $supplier->name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>

                        <label for="description">Description*</label>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="4" name="description">{{ old('description') }}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect">SAVE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Validation Plugin Js -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>

    <!-- bootstrap-select Js -->
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>

    <!-- froala editor Js -->
    <script src="{{ asset('js/backend/froala_editor.min.js') }}"></script>

    <!-- Additional JS -->
    <script type="text/javascript" src="{{ asset('js/backend/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/backend/purchase-orders.js') }}"></script>
@endsection
